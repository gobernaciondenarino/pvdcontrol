@extends('layouts.app')

@section('content')
<div class="container col-md-8 col-md-offset-4">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mt-3"><strong> Añadir administrador PVD</strong></h5>
            </div>

            <div class="card-body ">
                <form method="POST" action="{{ route('register') }}" id="registrar" class="needs-validation" novalidate>
                    @csrf

                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span for="name" class="input-group-text col-md-12" id="inputGroup-sizing-default" style="width: 154px">Nombre
                            </span>
                        </div>

                        <input type="text" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="name" value="{{ old('name') }}" required autofocus>
                        
                        <span id="name1" class="invalid-feedback" role="alert">

                        </span>                        

                    </div>

                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span for="email" class="input-group-text" id="inputGroup-sizing-default" style="width: 154px">{{ __('E-Mail') }}
                            </span>
                        </div>

                        <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"  aria-label="Default" aria-describedby="inputGroup-sizing-default" name="email" value="{{ old('email') }}" required>
                        
                        <span id="email1" class="invalid-feedback" role="alert">

                        </span>                        
                    </div>

                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span for="role" class="input-group-text" id="inputGroup-sizing-default" style="width: 154px">Role
                            </span>
                        </div>
                        {!! Form::select('role', ['admin' => 'Admin', 'admin_pvd' => 'Admin pvd'], null, ['class' => $errors->has('role') ? 'custom-select is-invalid' : 'custom-select', 'id' => 'role', 'placeholder' => 'Selecciona...']) !!}                        
                        <span id="role1" class="invalid-feedback" role="alert">

                        </span>                        
                    </div>

                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span for="municipio_id" class="input-group-text" id="inputGroup-sizing-default" >Municipio del PVD</span>
                        </div>
                        {!! Form::select('municipio_id', $mun, null, ['class' => $errors->has('municipio_id') ? 'custom-select is-invalid' : 'custom-select', 'id' => 'municipio_id', 'placeholder' => 'Selecciona...']) !!}                        
                        <span id="municipio_id1" class="invalid-feedback" role="alert">

                        </span>                        
                        
                    </div>

                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span for="password" class="input-group-text" id="inputGroup-sizing-default" style="width: 154px">{{ __('Password') }}</span>
                        </div>

                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required aria-label="Default" aria-describedby="inputGroup-sizing-default">
                        
                        <span id="password1" class="invalid-feedback" role="alert">

                        </span>                        
                    </div>

                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span for="password-confirm" class="input-group-text" id="inputGroup-sizing-default" >{{ __('Confirm Password') }}</span>
                        </div>
                        <input id="password-confirm" type="password" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" name="password_confirmation" required>
                    </div>

                    <button id="auth_añadir" class="btn btn-primary pull-right" style="width: 200px;">
                        {{ __('Añadir') }}
                    </button>                     
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
