@extends('welcome')

@section('content')

<div class="flex-center position-ref full-height">
  @if (Route::has('login'))
  @auth
  <div class="top-right links">
    <a href="{{ url('/home') }}" display="none">Hola {{Auth::user()->name}}</a>
  </div>
  @else
  
                    {{-- <a href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                        @endif --}}

                        <div class="content">

                          <body class="hold-transition login-page">
                            <div class="login-box">
                              <div class="login-logo">
                                <a><b>Pvd</b>Plus</a>
                              </div>
                              <!-- /.login-logo -->
                              <div class="card">
                                <div class="card-body login-card-body">
                                  <p class="login-box-msg">Inicia sesión</p>

                                  <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="input-group input-group-sm mb-3">
                                      <div class="input-group-prepend">
                                        <div class="input-group-text mw-100 p" id="inputGroup-sizing-sm">
                                          <label for="email" class="col-md-4 col-form-label text-md-right" style="width: 85px">Usuario</label>
                                        </div>
                                      </div>

                                      <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                      
                                      @if ($errors->has('email'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                      @endif
                                    </div>

                                    <div class="input-group input-group-sm mb-3">
                                      <div class="input-group-prepend">
                                        <div class="input-group-text" id="btnGroupAddon">
                                          <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>
                                        </div>
                                      </div>

                                      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                      @if ($errors->has('password'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                      @endif
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-block">Iniciar</button>

                                  </form>
                                </div>                        
                              </div>
                            </div>                                                         
                          </body>
                        </div>
                        @endauth
                        
                        @endif

                        
                      </div>
                      @endsection
