@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title mt-3 ml-4 mb-4"><strong>Reserva de salas</strong></h5>		
					<div class="card-body">

						{!! Form::open(['id' => 'form_reserva', 'class' => 'needs-validation', 'novalidate'] )!!}
                 		{{ method_field('POST') }}

						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="form-group">
											{!! Form::label('municipio_id', 'Municipio*',false)!!}
											{!! Form::select('municipio_id', $select_municipio, null, ['class' => 'form-control', 'id' => 'municipio_reserva', 'disabled']) !!}
										</div>

									</td>
									<td>
										<div class="form-group">
											{!! Form::label('pvd', 'Pvd del municipio*',false)!!}
											{!! Form::select('pvd', $select_pvd, null, ['class' => 'form-control', 'id' => 'pvd_reserva', 'disabled']) !!}
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('sala_id', 'Sala*',false)!!}											
											{!! Form::select('sala_id', $select_salas, null, ['class' => 'form-control', 'id' => 'sala_id', 'placeholder' => 'Selecciona...']) !!}
											<span id="sala_id1" class="invalid-feedback" role="alert">						    
											</span>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										{!! Form::label('fecha_inicio', 'Fecha inicio*',false)!!}
										<div class="form-group">
											<div class="input-group date" id="fecha_inicio4" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" data-target="#fecha_inicio4" id="fecha_inicio" name="fecha_inicio" />
												<div class="input-group-append" data-target="#fecha_inicio4" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="fa fa-calendar"></i></div>
												</div>
												<span id="fecha_inicio1" class="invalid-feedback" role="alert">						    
												</span>
											</div>
										</div>
									</td>
									<td colspan="2">
										{!! Form::label('fecha_fin', 'Fecha fin*',false)!!}
										<div class="form-group">
											<div class="input-group date" id="fecha_fin4" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" data-target="#fecha_fin4" id="fecha_fin" name="fecha_fin" />
												<div class="input-group-append" data-target="#fecha_fin4" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="fa fa-calendar"></i></div>
												</div>
												<span id="fecha_fin1" class="invalid-feedback" role="alert">						    
												</span>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										{!! Form::label('hora_inicio', 'Hora inicio*',false)!!}
										<div class="form-group">
											<div class="input-group date" id="hora_inicio3" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" data-target="#hora_inicio3" id="hora_inicio" name="hora_inicio" />
												<div class="input-group-append" data-target="#hora_inicio3" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="fa fa-clock-o"></i></div>
												</div>
												<span id="hora_inicio1" class="invalid-feedback" role="alert">						    
												</span>
											</div>
										</div>
									</td>
									
									<td colspan="2">
										{!! Form::label('hora_fin', 'Hora fin*',false)!!}
										<div class="form-group">
											<div class="input-group date" id="hora_fin3" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" data-target="#hora_fin3" id="hora_fin" name="hora_fin" />
												<div class="input-group-append" data-target="#hora_fin3" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="fa fa-clock-o"></i></div>
												</div>
												<span id="hora_fin1" class="invalid-feedback" role="alert">						    
												</span>
											</div>
										</div>
									</td>
								</tr>								
								<tr>
									<td>
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<div class="input-group-text">
													{!! Form::radio('facturable', 1, false,	['id' => 'exampleRadios1']) !!}
												</div>												
											</div>
											{!! Form::label('facturable', 'Facturable',['class' => 'form-control', 'for' => 'exampleRadios1', 'id' => 'facturable2'])!!}
										</div>
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<div class="input-group-text">
													{!! Form::radio('facturable', 0, false, ['id' => 'exampleRadios2']) !!}
												</div>												
											</div>
											{!! Form::label('facturable', 'No facturable',['class' => 'form-control', 'for' => 'exampleRadios2', 'id' => 'facturable'])!!}	
											<span id="facturable1" class="invalid-feedback" role="alert">						    
											</span>									
										</div>										
									</td>
									<td>
										{!! Form::label('tarifa', 'Tarifa*', ['class'=>'mt-4' ]) !!}
										{!! Form::text('tarifa', '', ['id' => 'tarifa', 'class' => 'form-control', 'disabled']) !!}
										
										<span id="tarifa1" class="invalid-feedback" role="alert">						    
											
										</span>
									</td>
									<td> 
										<div class="custom-control custom-checkbox"><br><br><br>							
											{!! Form::checkbox('factura', 'si',false, ['class' => 'custom-control-input', 'id' => 'factura']) !!}
											{!! Form::label('factura', '¿Desea generar factura?', ['class' => 'custom-control-label', 'id' => 'factura']) !!}
										</div>
									</td>

								</tr>
							</tbody>
						</table>

						{!! link_to('', 'Reservar', ['id'=>'guardar_reserva', 'class'=>'btn btn-success pull-right'], $secure=null) !!}
						{!! Form::submit('Volver', ['class' => 'btn btn-primary pull-right', 'onclick' => 'history.go(-1); return false;']) !!}

						{!! Form::close() !!}
					</div>
					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-body">
									<div class="tab-content">
										<strong>Listado de reservas</strong><br><br>
										<table class="table table-striped table-bordered table-sm" id="table_reservas"  style="width:100%">
											<thead>
												<tr class="bg-success">
													<th scope="col">Sala</th>													
													<th scope="col">Solicitante</th>
													<th scope="col">Fecha inicio</th>
													<th scope="col">Fecha fin</th>
													<th scope="col">Hora inicio</th>
													<th scope="col">Hora fin</th>
													<th scope="col">Facturable</th>
													<th scope="col">Tarifa</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	
	$(function () {
		$('#hora_inicio3').datetimepicker({
			format: 'LT',
			autoclose: true
		});

		$('#hora_fin3').datetimepicker({
			format: 'LT',
			autoclose: true
		});

		$('#fecha_inicio4').datetimepicker({
			format: 'L',
			autoclose: true

		});

		$('#fecha_fin4').datetimepicker({
			format: 'L',
			autoclose: true

		});
	});
</script>


@endsection
