@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title ml-4 mt-3"><strong> Agregar servicio al PVD</strong></h5>
					<div class="card-body">

						{!! Form::open(['id' => 'form_agregar_servicio_pvd', 'class' => 'needs-validation', 'novalidate'] )!!}
                 		{{ method_field('POST') }}
							
						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="form-group">
											{!! Form::label('municipio_id', 'Municipio*',false)!!}
											{!! Form::select('municipio_id', $select_municipio, null, ['class' => 'form-control', 'id' => 'municipio_id', 'disabled'] ) !!}
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('pvd', 'Pvd del municipio*',false)!!}
											{!! Form::select('pvd', $select_pvd, null, ['class' => 'form-control', 'id' => 'pvd', 'disabled']) !!}
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('servicio_id', 'Servicio*',false)!!}
											{!! Form::select('servicio_id', $servicios, null, ['class' => 'form-control', 'id' => 'servicio_id', 'placeholder' => 'Selecciona...']) !!}
											<span id="servicio_id1" class="invalid-feedback" role="alert">						    
											</span>
										</div>										
									</td>
									<td>
										{!! Form::label('tarifa', 'Tarifa*') !!}
										{!! Form::text('tarifa', '', ['id' => 'tarifa', 'class' => 'form-control']) !!}

										<span id="tarifa1" class="invalid-feedback" role="alert">							
										</span>
									</td>
								</tr>
							</tbody>
						</table>

						{!! link_to('', 'Agregar servicio', ['id'=>'agregar_servicio_pvd', 'class'=>'btn btn-success pull-right'], $secure=null) !!}
						{!! Form::submit('Volver', ['class' => 'btn btn-primary pull-right', 'onclick' => 'history.go(-1); return false;']) !!}
						{!! Form::close() !!}
					</div>

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-body">
									<div class="tab-content">
										<strong>Servicios del PVD</strong><br><br>
										<table class="table table-striped table-bordered table-sm" id="table_servicios_pvd"  style="width:100%">
											<thead>
												<tr class="bg-success">
													<th scope="col">Servicio</th>
													<th scope="col">Tarifa</th>
													<th scope="col">Acciones</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
