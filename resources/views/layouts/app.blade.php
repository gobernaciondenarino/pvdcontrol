<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Home</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="./plugins/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="./css/adminlte.min.css">
  <link rel="stylesheet" href="./plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="./plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="./plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  {{-- BOOTSTRAP --}}
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  {{-- TOAST ALERTAS --}}
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  
  {{-- FONT AWESOME --}}
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

  {{-- DATEPICKER --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />

  {{-- DATATABLES --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
  
  <!-- fullCalendar 2.2.5-->
  <link rel="stylesheet" href="./plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="./plugins/fullcalendar/fullcalendar.print.css" media="print">

  <style>
    .toast-error{
      background-color: red;
    }

    .toast-success{
      background-color: green;
    }
  </style>
</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="{{ route('home') }}" class="nav-link">Home</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fa fa-th-large"></i></a>
          </li>
        </ul>
      </nav>
      
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <a href="#" class="brand-link">
          <i class="fab fa-pied-piper-pp ml-4" style="font-size: 80px"></i>
          <strong>Pvd plus</strong>
        </a>
        <div class="sidebar">
          <div class="mt-3 pb-3 mb-3">
            <div class="info">
              <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                  <li class="nav-item has-treeview menu-close">
                    <a href="#" class="nav-link active">
                      <i class="fas fa-user-circle"></i>
                      @php
                      $value = str_limit( Auth::user()->name , 20);
                      @endphp

                      <p>
                        <strong>{{ $value }}</strong>
                      </p>
                      <i class="fa fa-angle-left right fas fa-caret-left"></i>

                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                       <a class="nav-link active" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                       <strong>Cerrar sesión</strong>
                     </a>

                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
                      @csrf
                    </form>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview menu-close">
            <a href="{{ route('home') }}" class="nav-link active">
              <i class="nav-icon fas fa-house-damage"></i>
              <p>
                <strong>Dashboard</strong>
                <i class="fa fa-angle-left right fas fa-caret-left"></i>                
              </p>
            </a>
          </li>
          
          @if(auth()->user()->role === 'admin_pvd')

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-hand-holding-usd"></i>
              <p>
                <strong>Facturar</strong>
                <i class="fa fa-angle-left right fas fa-caret-left"></i>                
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('uso_equipo.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-coins"></i>
                  <p>Facturar servicios</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clipboard"></i>
              <p>
                <strong>Reservas</strong>
                <i class="fa fa-angle-left right fas fa-caret-left"></i>                
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('reserva.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-clipboard-check"></i>
                  
                  <p>Asignar reserva</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-database"></i>

              <p>
                <strong>Generar reportes</strong>
                <i class="fa fa-angle-left right fas fa-caret-left"></i>                
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('reporte_uso_equipo_pvd') }}" class="nav-link">
                  <i class="nav-icon fas fa-file-download"></i>
                  <p>Reporte de uso de</p><br>    
                  <p style="margin-left: 32px">servicios</p> 
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('reporte_reserva_pvd') }}" class="nav-link">
                  <i class="nav-icon fas fa-file-download"></i>
                  <p>Reporte de reservas</p>
                </a>
              </li>
            </ul>
          </li>

          @else  

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-clipboard"></i>
              <p><strong>Seguimiento de uso</strong></p><br>
              <p style="margin-left: 32px"><strong>de servicios</strong>
                <i class="fa fa-angle-left right fas fa-caret-left"></i>                          
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('seguimiento_uso.index')}}" class="nav-link">
                  <i class="nav-icon far fa-eye"></i>
                  <p>Realizar filtro</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-database"></i>

              <p>
                <strong>Generar reportes</strong>
                <i class="fa fa-angle-left right fas fa-caret-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('reporte_uso_equipo_total') }}" class="nav-link">
                  <i class="nav-icon fas fa-file-download"></i>
                  <p>Reporte de uso de</p><br>    
                  <p style="margin-left: 32px">servicios</p> 
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('reporte_reserva_total') }}" class="nav-link">
                  <i class="nav-icon fas fa-file-download"></i>
                  <p>Reporte de reservas</p>
                </a>
              </li>
            </ul>
          </li>

          @endif

          
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cogs"></i>
              <p>
                <strong>Configuración</strong>
                <i class="fa fa-angle-left right fas fa-caret-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="margin-bottom: 80px;">

              {{-- VISTA DEL ADMIN //////////////////////////////////// --}}

              @if(auth()->user()->role === 'admin')
              <li class="nav-item">
                <a href="{{ route('mun_pvd.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-file-upload"></i>
                  <p>Cargar municipios y pvds</p>
                </a>
              </li> 

              <li class="nav-item">
                <a href="{{ route('register') }}" class="nav-link">
                  <i class="nav-icon fas fa-user-plus"></i>
                  <p>Agregar Admin PVD</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{ route('servicio.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-plus"></i>
                  <p>Crear un servicio</p>
                </a>
              </li>

              @else              
              {{-- VISTA ADMIN PVD --}}
              <li class="nav-item">
                <a href="{{ route('agregar_s_pvd.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-book-reader"></i>
                  <p>Manejo de servicios</p><br>
                  <p style="margin-left: 32px">del PVD</p>                      
                </a>
              </li> 
              <li class="nav-item">
                <a href="{{ route('crear_sala.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-book-reader"></i>
                  <p>Manejo de salas</p><br>
                  <p style="margin-left: 32px"> del PVD</p>                      
                </a>
              </li> 
              <li class="nav-item">
                <a href="{{ route('crear_inventario.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-book-reader"></i>
                  <p>Manejo de inventarios</p><br>
                  <p style="margin-left: 32px"> del PVD</p>                      
                </a>
              </li> 

              <li class="nav-item">
                <a href="{{ route('sala.index') }}" class="nav-link">
                  <i class="nav-icon fas fa-file-upload"></i>                  
                  <p>Cargar salas </p><br>    
                  <p style="margin-left: 32px">e inventarios al PVD</p>                  
                </a>
              </li>   
              @endif
            </ul>
          </li>
        </ul>
      </li>
    </ul>
  </nav>
</div>
</aside>

<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          @yield('titulo')
        </div>
      </div>
    </div>
  </div>

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        @yield('content')
      </div>
    </div>
  </section>
</div>
<footer class="main-footer">
  <strong>Copyright &copy; 2014-2018 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
  All rights reserved.
</footer>

<aside class="control-sidebar control-sidebar-dark">
</aside>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<script src="./plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="./plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="./plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="./plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

<script src="./plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="./plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="./plugins/fastclick/fastclick.js"></script>
<script src="./js/adminlte.js"></script>
<script src="./js/demo.js"></script>


{{-- DATEPICKER --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>

{{-- DATATABLES --}}
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

{{-- TOAST ALERTAS --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

{{-- BOOTBOX --}}
<script src="./js/bootbox.min.js"></script>

{{-- JQ FORMS --}}
<script src="http://malsup.github.com/jquery.form.js"></script> 

<!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="./plugins/fullcalendar/fullcalendar.min.js"></script>

{{-- LOGICA --}}
<script src="./js/logica/logica_uso_equipo.js"></script>
<script src="./js/logica/logica_servicio.js"></script>
<script src="./js/logica/logica_agregar_s_pvd.js"></script>
<script src="./js/logica/logica_seguimiento_uso.js"></script>
<script src="./js/logica/logica_auth_registrar.js"></script>
<script src="./js/logica/logica_reservas.js"></script>
<script src="./js/logica/logica_cargar_salas.js"></script>
<script src="./js/logica/logica_cargar_mun_pvd.js"></script>
<script src="./js/logica/logica_salas.js"></script>
<script src="./js/logica/logica_inventarios.js"></script>

<script>
  window.laravel = {!!
    json_encode(['url' => URL::to('/'), 'token' => csrf_token()])  
    !!};
  </script>

</body>
</html>
