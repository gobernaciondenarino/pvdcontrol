<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

</head>
<body>
	
 <div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Reporte general</h4>
  <p>Reporte de todas las reservas</p>
  
  <hr>
  
  <div class="d-flex flex-row bd-highlight" style="margin-bottom: -40px;">
    <div class="bd-highlight mb-5">Fecha: {{ $factual }}</div>
  </div>
</div>

<table class="table table-bordered rounded">
  <thead class="table-active" >
    <tr>
      <th scope="col">Pvd</th>
      <th scope="col">Sala</th>
      <th scope="col">Solicitante</th>
      <th scope="col">Fecha inicio</th>
      <th scope="col">Fecha fin</th>
      <th scope="col">Hora inicio</th>
      <th scope="col">Hora fin</th>
      <th scope="col">Tarifa</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($pvds as $pvd)
      @foreach ($pvd->salas as $sala)
        @foreach ($sala->reservas as $reserva)
          <tr>
            <td>{{ $pvd->nombre }}</td>
            <td>{{ $sala->nombre }}</td>
            <td>{{ $reserva->solicitante }}</td>
            <td> {{ $reserva->fecha_inicio }} </td>
            <td> {{ $reserva->fecha_fin }} </td>
            <td> {{ $reserva->hora_inicio }} </td>
            <td> {{ $reserva->hora_fin }} </td>
            
            @php
              if($reserva->tarifa==null) {
                $tarifa = 'No facturable';
              }else{
                $tarifa = '$ '.number_format($reserva->tarifa,0,'.', '.');
              }
            @endphp      
            <td>{{ $tarifa}} </td>      
          </tr>
        @endforeach
      @endforeach
    @endforeach
  </tbody>
</table>
</body>
</html>