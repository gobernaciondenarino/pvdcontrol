<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

</head>
<body>
	
 <div class="alert alert-success" role="alert">
  <h4 class="alert-heading">{{ $pvd->nombre}}</h4>
  <p> Reporte de reservas </p>
  
  <hr>
  
  <div class="d-flex flex-row bd-highlight" style="margin-bottom: -40px;">
    <div class="bd-highlight">Fecha: {{ $factual }}</div>
    <div class="bd-highlight" style="margin-left: 38%;">Dirección: {{ $pvd->direccion}}</div>
    <div class="bd-highlight" style="margin-left: 82%;">Telefono: {{ $pvd->celular}}</div>
  </div>
</div>

<table class="table table-bordered rounded">
  <thead class="table-active" >
    <tr>
      <th scope="col">Sala</th>
      <th scope="col">Solicitante</th>
      <th scope="col">Fecha inicio</th>
      <th scope="col">Fecha fin</th>
      <th scope="col">Hora inicio</th>
      <th scope="col">Hora fin</th>
      <th scope="col">Tarifa</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($reservas as $reserva)
    <tr>
      <td>{{ $reserva->sala->nombre }}</td>
      <td>{{ $reserva->solicitante }}</td>
      <td> {{ $reserva->fecha_inicio }} </td>
      <td> {{ $reserva->fecha_fin }} </td>
      <td> {{ $reserva->hora_inicio }} </td>
      <td> {{ $reserva->hora_fin }} </td>
      
      @php
        if($reserva->tarifa==null) {
          $tarifa = 'No facturable';
        }else{
          $tarifa = '$ '.number_format($reserva->tarifa,0,'.', '.');
        }
      @endphp      
      <td>{{ $tarifa}} </td>      
    </tr>
    @endforeach
  </tbody>
</table>


</body>
</html>