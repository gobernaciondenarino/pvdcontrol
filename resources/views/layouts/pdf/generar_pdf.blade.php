<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

</head>
<body>
	
 <div class="alert alert-success" role="alert">
  <h4 class="alert-heading">{{ $pvd->nombre}}</h4>
  <p> Factura No. {{ $uso->id }} </p>
  
  <hr>
  
  @php
  use Carbon\Carbon;
  $a = Carbon::parse($uso->fecha_hora_inicio);
  @endphp
  
  <div class="d-flex flex-row bd-highlight" style="margin-bottom: -40px;">
    <div class="bd-highlight">Fecha: {{ $a->format('Y'.'/'.'m'.'/'.'d')}}</div>
    <div class="bd-highlight" style="margin-left: 38%;">Dirección: {{ $pvd->direccion}}</div>
    <div class="bd-highlight" style="margin-left: 82%;">Telefono: {{ $pvd->celular}}</div>
  </div>
</div>

<table class="table table-bordered rounded">
  <thead class="table-active" >
    <tr>
      <th scope="col">#</th>
      <th scope="col">Servicio</th>
      <th scope="col">Fecha</th>
      @if($servicio->equipo)
      <th scope="col">Hora inicio</th>
      <th scope="col">Hora fin</th>
      @else
      <th scope="col">Cantidad</th>
      @endif
      <th scope="col">Tarifa</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>{{ $servicio->nombre_servicio}}</td>
      <td>{{ $uso->fecha}}</td>
      @if($servicio->equipo)
      <td> {{ $uso->hora_inicio }} </td>
      <td> {{ $uso->hora_fin }} </td>
      @else
      <td> {{$uso->cantidad }} </td>
      @endif
      
      @php
      $tarifa = number_format($pvd_servicio->tarifa,0,'.', '.');
      $total = number_format($uso->total,0,'.', '.');
      @endphp
      <td>$ {{ $tarifa }} </td>
    </tr>
    <tr>
      @if($servicio->equipo)
      <th colspan="4"></th>
      @else
      <th colspan="3"></th>
      @endif

      <th scope="col" class="alert-success">Total</th>
      <th scope="col" class="alert-success">$ {{$total}}</th>

    </tr>
  </tbody>
</table>


</body>
</html>