<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

</head>
<body>
	
 <div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Reporte general</h4>
  <p>Reporte de todos los usos de servicios</p>  
  
  <hr>
  
  <div class="d-flex flex-row bd-highlight" style="margin-bottom: -40px;">
    <div class="bd-highlight mb-5">Fecha: {{ $factual }}</div>
  </div>
</div>

<table class="table table-bordered rounded">
  <thead class="table-active" >
    <tr>
      <th scope="col">Pvd</th>
      <th scope="col">Servicio</th>
      <th scope="col">Cantidad</th>
      <th scope="col">Fecha</th>
      <th scope="col">Hora inicio</th>
      <th scope="col">Hora fin</th>
      <th scope="col">Tarifa</th>
      <th scope="col">Total</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($pvd_servicios as $ps)
      @foreach ($ps->uso_equipos as $uso)
      <tr>
        <td>{{ $ps->pvd->nombre }}</td>
        <td>{{ $ps->servicio->nombre_servicio }}</td>
        <td>{{ $uso->cantidad }}</td>
        <td>{{ $uso->fecha }} </td>
        
        @php
        if ($uso->hora_inicio == '00:00:00') {
          $hora_inicio = '---';
          $hora_fin = '---';
        }else{
          $hora_inicio = $uso->hora_inicio;
          $hora_fin = $uso->hora_fin;
        }
        @endphp
        <td> {{ $hora_inicio }} </td>
        <td> {{ $hora_fin }} </td>
        
        @php

        $tarifa = '$ '.number_format($uso->tarifa,0,'.', '.');
        $total = '$ '.number_format($uso->total,0,'.', '.');
        @endphp      
        <td>{{ $tarifa}} </td>      
        <td>{{ $total}} </td>      
      </tr>
      @endforeach
    @endforeach
  </tbody>
</table>
</body>
</html>