@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title ml-3 mt-3"><strong> Crear servicio</strong></h5>
					<div class="card-body">

						{!! Form::open(['id' => 'form_crear_servicio', 'class' => 'needs-validation', 'novalidate'] )!!}
                 		{{ method_field('POST') }}
							
						<table class="table">
							<tbody>
								<tr>
									<td>
										{!! Form::label('cod_servicio', 'Codigo servicio*') !!}
										{!! Form::text('cod_servicio', '', ['id' => 'cod_servicio', 'class' => 'form-control']) !!}
										<span id="cod_servicio1" class="invalid-feedback" role="alert">						    
										</span>
									</td>
									<td>
										{!! Form::label('nombre_servicio', 'Nombre servicio*') !!}
										{!! Form::text('nombre_servicio', '', ['id' => 'nombre_servicio', 'class' => 'form-control']) !!}

										<span id="nombre_servicio1" class="invalid-feedback" role="alert">							
										</span>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('equipo', 'Es equipo*',false)!!}
											{!! Form::select('equipo', ['1' => 'Si', '0' => 'No'], null, ['class' => 'form-control', 'id' => 'equipo', 'placeholder' => 'Selecciona...']) !!}
											<span id="equipo1" class="invalid-feedback" role="alert">						    
											</span>
										</div>										
									</td>
								</tr>
							</tbody>
						</table>

						{!! link_to('', 'Crear servicio', ['id'=>'guardar_servicio', 'class'=>'btn btn-success pull-right'], $secure=null) !!}
						{!! Form::submit('Volver', ['class' => 'btn btn-primary pull-right', 'onclick' => 'history.go(-1); return false;']) !!}
						{!! Form::close() !!}
					</div>

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-body">
									<div class="tab-content">
										<strong>Listado de servicio</strong><br><br>
										<table class="table table-striped table-bordered table-sm" id="table_servicios"  style="width:100%">
											<thead>
												<tr class="bg-success">
													<th scope="col">Codigo servicio</th>
													<th scope="col">Nombre servicio</th>
													<th scope="col">Es equipo</th>
													<th scope="col"></th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
