@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title ml-4 mt-3"><strong> Facturar un servicio</strong></h5>					
					<div class="card-body">
						{!! Form::open(['id' => 'form_uso_equipo', 'class' => 'needs-validation', 'novalidate'] )!!}
						<table class="table">
							<tbody>
								<tr>
									<td colspan="2"> 
										<div class="form-group">
											{!! Form::label('municipio_id', 'Municipio*',false)!!}
											{!! Form::select('municipio_id', $select_municipio, null, ['class' => 'form-control', 'id' => 'municipio_id', 'disabled'] ) !!}
										</div>
									</td>
									<td colspan="2">
										<div class="form-group">
											{!! Form::label('pvd', 'Pvd del municipio*',false)!!}
											{!! Form::select('pvd', $select_pvd, null, ['class' => 'form-control', 'id' => 'pvd', 'disabled']) !!}
										</div>
									</td>									
								</tr>
								<tr>
									<td colspan="2">
										<div class="form-group">
											{!! Form::label('servicio', 'Servicio*',false)!!}
											{!! Form::select('servicio', $select_servicio, null, ['class' => 'custom-select', 'id' => 'servicio', 'placeholder' => 'Selecciona...']) !!}
											<span id="servicio1" class="invalid-feedback" role="alert">

											</span>
										</div>										
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('tiempo', 'Tiempo*',false)!!}
											{!! Form::select('tiempo', [], null, ['class' => 'custom-select', 'id' => 'tiempo', 'placeholder' => 'Selecciona...', 'disabled']) !!}
											<span id="tiempo1" class="invalid-feedback" role="alert">

											</span>
										</div>
									</td>
									<td>
										{!! Form::label('cantidad', 'Cantidad*') !!}
										{!! Form::text('cantidad', '', ['id' => 'cantidad', 'class' => 'form-control', 'disabled']) !!}
										<span id="cantidad1" class="invalid-feedback" role="alert">

										</span>
									</td>									
								</tr>
								<tr>
									<td colspan="2">
										{!! Form::label('hora_inicio', 'Hora inicio*',false)!!}
										<div class="form-group">
											<div class="input-group date" id="hora_inicio3" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" data-target="#hora_inicio3" id="hora_inicio" name="hora_inicio" disabled="disabled"/>
												<div class="input-group-append" data-target="#hora_inicio3" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="fa fa-clock-o"></i></div>
												</div>
											</div>
										</div>
									</td>
									<td colspan="2">
										{!! Form::label('hora_fin', 'Hora fin*',false)!!}
										<div class="form-group">
											<div class="input-group date" id="hora_fin3" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" data-target="#hora_fin3" id="hora_fin" name="hora_fin" disabled="disabled"/>
												<div class="input-group-append" data-target="#hora_fin3" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="fa fa-clock-o"></i></div>
												</div>
											</div>
										</div>
									</td>								
								</tr>
								
								<tr>
									<td></td>
									<td></td>
									<td>																			
										{!! Form::label('tarifa', 'Tarifa*') !!}									
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">$</span>
										  </div>
										  {!! Form::text('tarifa', '', ['id' => 'tarifa', 'class' => 'form-control']) !!}
										</div>
										<span id="tarifa1" class="invalid-feedback" role="alert">

										</span>
									</td>
									<td> 
										<div class="custom-control custom-checkbox"><br><br>							
											{!! Form::checkbox('factura', 'si',false, ['class' => 'custom-control-input', 'id' => 'factura']) !!}
											{!! Form::label('factura', '¿Desea generar factura?', ['class' => 'custom-control-label', 'id' => 'factura']) !!}
										</div>
									</td>
								</tr>
							</tbody>
						</table>

						{!! link_to('', 'Facturar', ['id'=>'guardar_uso', 'class'=>'btn btn-success pull-right'], $secure=null) !!}
						{!! Form::submit('Volver', ['class' => 'btn btn-primary pull-right', 'onclick' => 'history.go(-1); return false;']) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	
	$(function () {
		$('#hora_inicio3').datetimepicker({
			format: 'LT',
			autoclose: true
		});

		$('#hora_fin3').datetimepicker({
			format: 'LT',
			autoclose: true
		});
	});
</script>

@endsection
