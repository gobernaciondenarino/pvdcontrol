@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title mt-3 ml-4 mb-4"><strong>Cargar datos y crear salas</strong></h5>		
					<div class="card-body">
						{!! Form::open(['id' => 'form_cargar_salas', 'enctype' => 'multipart/form-data', 'class' => 'needs-validation', 'novalidate'] )!!}

							{!! Form::label('title', 'Archivo',false)!!}
							<div class="custom-file">
								<input type="file" name="archivo" class="custom-file-input" id="archivo">
								<label class="custom-file-label" for="archivo">Elige un archivo</label>
								<span id="archivo1" class="invalid-feedback" role="alert">						    
								</span>
							</div>
							

							{!! link_to('#', 'Cargar', ['id'=>'cargar_salas', 'class'=>'btn btn-primary mt-1 pull-right', 'style' => 'width: 78px'], $secure=null) !!}
						{!! Form::close() !!}

					</form>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-body">
				<h5 class="card-title mt-3 ml-4 mb-4"><strong>Cargar inventario</strong></h5>		
				<div class="card-body">
					{!! Form::open(['id' => 'form_cargar_inventario', 'enctype' => 'multipart/form-data', 'class' => 'needs-validation', 'novalidate'] )!!}						
						<table class="table">
							<tbody>
								<tr>	
									<td>
										<div class="form-group">
											{!! Form::label('salas', 'Salas',false)!!}
											{!! Form::select('sala', $salas, null, ['class' => 'form-control', 'id' => 'sala', 'placeholder' => 'Selecciona...']) !!}
											<span id="sala1" class="invalid-feedback" role="alert">						    
											</span>

										</div>										
									</td>								
									<td>											
										{{csrf_field()}}
										{!! Form::label('title', 'Archivo',false)!!}
										<div class="custom-file" >
											<input type="file" name="inventario" class="custom-file-input" id="inventario">
											<label class="custom-file-label" for="inventario">Elige un archivo</label>
											<span id="inventario1" class="invalid-feedback" role="alert">						    
											</span>
										</div>
										{!! link_to('#', 'Cargar', ['id'=>'cargar_inventario', 'class'=>'btn btn-primary mt-1 pull-right', 'style' => 'width: 78px'], $secure=null) !!}
									</td>									
								</tr>
							</tbody>
						</table>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
</div>

@endsection