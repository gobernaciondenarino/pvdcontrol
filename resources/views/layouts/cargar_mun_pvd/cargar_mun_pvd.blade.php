@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title mt-3 ml-4 mb-4"><strong>Cargar municipios y sus pvds</strong></h5>		
					<div class="card-body">
						{!! Form::open(['id' => 'form_cargar_mun_pvd', 'enctype' => 'multipart/form-data', 'class' => 'needs-validation', 'novalidate'] )!!}

							{!! Form::label('title', 'Archivo',false)!!}
							<div class="custom-file">
								<input type="file" name="mun_pvd" class="custom-file-input" id="mun_pvd">
								<label class="custom-file-label" for="mun_pvd">Elige un archivo</label>
								<span id="mun_pvd1" class="invalid-feedback" role="alert">						    
								</span>
							</div>
							

							{!! link_to('#', 'Cargar', ['id'=>'cargar_mun_pvd', 'class'=>'btn btn-primary mt-1 pull-right', 'style' => 'width: 78px'], $secure=null) !!}
						{!! Form::close() !!}

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

@endsection