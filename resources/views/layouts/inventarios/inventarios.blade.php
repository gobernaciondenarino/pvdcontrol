@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title ml-4 mt-3"><strong> Agregar el inventario a una sala</strong></h5>
					<div class="card-body">

						{!! Form::open(['id' => 'form_crear_inventario', 'class' => 'needs-validation', 'novalidate'] )!!}
						{{ method_field('POST') }}

						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="form-group">
											{!! Form::label('municipio_id', 'Municipio',false)!!}
											{!! Form::select('municipio_id', $select_municipio, null, ['class' => 'form-control', 'id' => 'municipio_id', 'disabled'] ) !!}
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('pvd', 'Pvd del municipio',false)!!}
											{!! Form::select('pvd', $select_pvd, null, ['class' => 'form-control', 'id' => 'pvd', 'disabled']) !!}
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('sala_id', 'Sala',false)!!}
											{!! Form::select('sala_id', $select_salas, null, ['class' => 'form-control', 'id' => 'sala_id', 'placeholder' => 'Selecciona...']) !!}

											<span id="sala_id1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('nombre_elemento', 'Nombre elemento') !!}
											{!! Form::text('nombre_elemento', '', ['id' => 'nombre_elemento', 'class' => 'form-control']) !!}

											<span id="nombre_elemento1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>	
								</tr>

								<tr>
									
									<td>
										<div class="form-group"><br>
											{!! Form::label('unidad', 'Unidad') !!}
											{!! Form::text('unidad', '', ['id' => 'unidad', 'class' => 'form-control']) !!}

											<span id="unidad1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>
									<td>
										<div class="form-group"><br>
											{!! Form::label('cantidad_inv', 'Cantidad') !!}
											{!! Form::text('cantidad_inv', '', ['id' => 'cantidad_inv', 'class' => 'form-control']) !!}

											<span id="cantidad_inv1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('cantidad_elem_mant_prev', 'Cant. de elementos en mantenimiento preventivo') !!}
											{!! Form::text('cantidad_elem_mant_prev', '', ['id' => 'cantidad_elem_mant_prev', 'class' => 'form-control']) !!}

											<span id="cantidad_elem_mant_prev1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>
									<td>
										<div class="form-group"><br>
											{!! Form::label('observaciones', 'Observaciones') !!}
											{!! Form::text('observaciones', '', ['id' => 'observaciones', 'class' => 'form-control']) !!}

											<span id="observaciones1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>
								</tr>

								<tr>
									<td>
										<div class="form-group">
											{!! Form::label('tipo_equipo', 'Tipo equipo') !!}
											{!! Form::text('tipo_equipo', '', ['id' => 'tipo_equipo', 'class' => 'form-control']) !!}

											<span id="tipo_equipo1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('marca', 'Marca') !!}
											{!! Form::text('marca', '', ['id' => 'marca', 'class' => 'form-control']) !!}

											<span id="marca1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('serial', 'Serial') !!}
											{!! Form::text('serial', '', ['id' => 'serial', 'class' => 'form-control']) !!}

											<span id="serial1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('placa', 'Placa') !!}
											{!! Form::text('placa', '', ['id' => 'placa', 'class' => 'form-control']) !!}

											<span id="placa1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>
								</tr>

								<tr>
									<td>
										<div class="form-group">
											{!! Form::label('valor', 'Valor') !!}
											{!! Form::text('valor', '', ['id' => 'valor', 'class' => 'form-control']) !!}

											<span id="valor1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('valor_iva', 'Valor iva') !!}
											{!! Form::text('valor_iva', '', ['id' => 'valor_iva', 'class' => 'form-control']) !!}

											<span id="valor_iva1" class="invalid-feedback" role="alert">							
											</span>
										</div>
									</td>									
								</tr>
							</tbody>
						</table>

						{!! link_to('#', 'Agregar inventario', ['id'=>'crear_inventario', 'class'=>'btn btn-success pull-right'], $secure=null) !!}
						{!! Form::submit('Volver', ['class' => 'btn btn-primary pull-right', 'onclick' => 'history.go(-1); return false;']) !!}
						{!! Form::close() !!}
					</div>

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-body">
									<div class="tab-content">
										<strong>Inventarios de salas del PVD</strong><br><br>
										<table class="table table-striped table-bordered table-sm" id="table_inventario"  style="width:100%">
											<thead>
												<tr class="bg-success">
													<th scope="col">Sala</th>
													<th scope="col">Nombre elemento</th>
													<th scope="col">Cantidad</th>
													<th scope="col">Cant. elemento mant. preventivo</th>
													<th scope="col">Observaciones</th>
													<th scope="col">Acciones</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
