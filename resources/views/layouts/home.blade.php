@extends('layouts.app')

@section('content')

@if(auth()->user()->role === 'admin_pvd')

<section class="content">
	<div class="container-fluid">
		<div class="row">         
			<div class="card card-primary ml-3 mr-3">
				<h5 class="card-title ml-3 mt-5"><strong> Calendario de uso de servicios</strong></h5>
				<div class="card-body p-0">
					<div id="calendar"></div>
				</div>             
			</div>   
			<div class="card card-primary ml-3 mr-3">
				<h5 class="card-title ml-3 mt-5"><strong> Calendario de reservas</strong></h5>
				<div class="card-body p-0">
					<div id="calendar2"></div>					
				</div>             
			</div>       
		</div>
	</div>
</section>	

@else

<section class="content">
	<div class="container-fluid">
		<div class="row">         
			<div class="card card-primary ml-3 mr-3">
				<h5 class="card-title ml-3 mt-5"><strong> Calendario de uso de servicios de todos los pvds</strong></h5>
				<div class="card-body p-0">
					<div id="calendar3"></div>
				</div>             
			</div>   
			<div class="card card-primary ml-3 mr-3">
				<h5 class="card-title ml-3 mt-5"><strong> Calendario de reservas de todos los pvds</strong></h5>
				<div class="card-body p-0">
					<div id="calendar4"></div>					
				</div>             
			</div>       
		</div>
	</div>
</section>	

@endif

<script>
	$(function () {
		@php
			use Carbon\Carbon;
		@endphp

		$('#calendar').fullCalendar({
			height: 400,			
			header    : {
				left  : 'prev,next today',
				center: 'title',
				right : 'month,agendaWeek,agendaDay'
			},
			buttonText: {
				today: 'today',
				month: 'month',
				week : 'week',
				day  : 'day'	
			},
		  	events    : [
		  	@foreach($usos as $uso)
			  	@php		
					$date = Carbon::parse($uso->fecha);				
					$año = $date->year;
					$mes = $date->month;
					$dia = $date->day;

					$hi = Carbon::parse($uso->hora_inicio);
					$horai = $hi->hour;
					$minutoi = $hi->minute;
					$segundoi = $hi->second;
					$hf = Carbon::parse($uso->hora_fin);
					$horaf = $hf->hour;
					$minutof = $hf->minute;
					$segundof = $hf->second;
			  	@endphp			  	
			  	{
				  	title          : '{{ $uso->pvd_servicio->servicio->nombre_servicio }}',
				  	start          : new Date({{$año}}, {{$mes}}-1, {{$dia}}, {{$horai}}, {{$minutoi}}, {{$segundoi}}),
				  	end            : new Date({{$año}}, {{$mes}}-1, {{$dia}}, {{$horaf}}, {{$minutof}}, {{$segundof}}),
				  	allDay         : false,
			      	backgroundColor: 'hsl('+Math.floor(Math.random()*360)+',100%, 70%)',
			    	borderColor    : '#FFFFFF',			    	
			  	},
		  	@endforeach
		  	],
		  	editable  : false,
		  	droppable : true
		});

		$('#calendar2').fullCalendar({
			height: 400,			
			header    : {
				left  : 'prev,next today',
				center: 'title',
				right : 'month,agendaWeek,agendaDay'
			},
			buttonText: {
				today: 'today',
				month: 'month',
				week : 'week',
				day  : 'day'
			},
		  	events    : [
		  	@foreach($reservas as $reserva)
			  	@php		
					$date = Carbon::parse($reserva->fecha_inicio);				
					$año = $date->year;
					$mes = $date->month;
					$dia = $date->day;

					$date2 = Carbon::parse($reserva->fecha_fin);				
					$año2 = $date2->year;
					$mes2 = $date2->month;
					$dia2 = $date2->day;

					$hi = Carbon::parse($reserva->hora_inicio);
					$horai = $hi->hour;
					$minutoi = $hi->minute;
					$segundoi = $hi->second;
					
					$hf = Carbon::parse($reserva->hora_fin);
					$horaf = $hf->hour;
					$minutof = $hf->minute;
					$segundof = $hf->second;					
			  	@endphp			  	
			  	{
				  	title          : '{{ $reserva->sala->nombre }}',
				  	start          : new Date({{$año}}, {{$mes}}-1, {{$dia}}, {{$horai}}, {{$minutoi}}, {{$segundoi}}),
				  	end            : new Date({{$año2}}, {{$mes2}}-1, {{$dia2}}, {{$horaf}}, {{$minutof}}, {{$segundof}}),
				  	allDay         : false,
				    backgroundColor: 'hsl('+Math.floor(Math.random()*360)+',100%, 70%)',
				    borderColor    : '#FFFFFF',	
			  	},
		  	@endforeach
		  	],
		  	editable  : false,
		  	droppable : true
		});

		$('#calendar3').fullCalendar({
			height: 400,			
			header    : {
				left  : 'prev,next today',
				center: 'title',
				right : 'month,agendaWeek,agendaDay'
			},
			buttonText: {
				today: 'today',
				month: 'month',
				week : 'week',
				day  : 'day'
			},
		  	events    : [
    		@foreach ($pvd_servicios as $ps)
			  	@foreach ($ps->uso_equipos as $uso)
				  	@php		
						$date = Carbon::parse($uso->fecha);				
						$año = $date->year;
						$mes = $date->month;
						$dia = $date->day;

						$hi = Carbon::parse($uso->hora_inicio);
						$horai = $hi->hour;
						$minutoi = $hi->minute;
						$segundoi = $hi->second;
						$hf = Carbon::parse($uso->hora_fin);
						$horaf = $hf->hour;
						$minutof = $hf->minute;
						$segundof = $hf->second;
				  	@endphp			  	
				  	{
					  	title          : '{{ $uso->pvd_servicio->pvd->nombre.' - '. $uso->pvd_servicio->servicio->nombre_servicio }}',
					  	start          : new Date({{$año}}, {{$mes}}-1, {{$dia}}, {{$horai}}, {{$minutoi}}, {{$segundoi}}),
					  	end            : new Date({{$año}}, {{$mes}}-1, {{$dia}}, {{$horaf}}, {{$minutof}}, {{$segundof}}),
					  	allDay         : false,
				      	backgroundColor: 'hsl('+Math.floor(Math.random()*360)+',100%, 70%)',
				    	borderColor    : '#FFFFFF',			    	
				  	},
			  	@endforeach
		  	@endforeach
		  	],
		  	editable  : false,
		  	droppable : true
		});

		$('#calendar4').fullCalendar({
			height: 400,			
			header    : {
				left  : 'prev,next today',
				center: 'title',
				right : 'month,agendaWeek,agendaDay'
			},
			buttonText: {
				today: 'today',
				month: 'month',
				week : 'week',
				day  : 'day'
			},
		  	events    : [
		  	@foreach ($pvds as $pvd)
		  	  @foreach ($pvd->salas as $sala)
		  	    @foreach ($sala->reservas as $reserva)
			  	@php		
					$date = Carbon::parse($reserva->fecha_inicio);				
					$año = $date->year;
					$mes = $date->month;
					$dia = $date->day;

					$date2 = Carbon::parse($reserva->fecha_fin);				
					$año2 = $date2->year;
					$mes2 = $date2->month;
					$dia2 = $date2->day;

					$hi = Carbon::parse($reserva->hora_inicio);
					$horai = $hi->hour;
					$minutoi = $hi->minute;
					$segundoi = $hi->second;
					
					$hf = Carbon::parse($reserva->hora_fin);
					$horaf = $hf->hour;
					$minutof = $hf->minute;
					$segundof = $hf->second;					
			  	@endphp			  	
			  	{
				  	title          : '{{$pvd->nombre.' - '. $reserva->sala->nombre}}',
				  	start          : new Date({{$año}}, {{$mes}}-1, {{$dia}}, {{$horai}}, {{$minutoi}}, {{$segundoi}}),
				  	end            : new Date({{$año2}}, {{$mes2}}-1, {{$dia2}}, {{$horaf}}, {{$minutof}}, {{$segundof}}),
				  	allDay         : false,
				    backgroundColor: 'hsl('+Math.floor(Math.random()*360)+',100%, 70%)',
				    borderColor    : '#FFFFFF',	
			  	},
		  	@endforeach
		  	@endforeach
		  	@endforeach
		  	],
		  	editable  : false,
		  	droppable : true
		});

				
		
	});
</script>
@endsection
