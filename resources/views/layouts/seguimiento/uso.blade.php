@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title mt-3 ml-4 mb-4"><strong>Seguimiento de uso de servicios</strong></h5>					
					<div class="card-body">

						{!! Form::open(['id' => 'form_seguimiento_uso', 'class' => 'needs-validation', 'novalidate'] )!!}
                 		{{ method_field('POST') }}
							
						<table class="table">
							<tbody>
								<tr>
									<td>
										<div class="form-group">
											{!! Form::label('pvd_id', 'Pvd',false)!!}
											{!! Form::select('pvd_id', $pvds, null, ['class' => 'form-control', 'id' => 'pvd_id', 'placeholder' => 'Selecciona...']) !!}
											<span id="pvd_id1" class="invalid-feedback" role="alert">						    
											</span>
										</div>
									</td>
									<td>
										<div class="form-group">
											{!! Form::label('servicio_id', 'Servicio',false)!!}
											{!! Form::select('servicio_id', $servicios, null, ['class' => 'form-control', 'id' => 'servicio_id', 'placeholder' => 'Selecciona...']) !!}
											<span id="servicio_id1" class="invalid-feedback" role="alert">						    
											</span>
										</div>
									</td>
									<td>
										{!! Form::label('filtro', 'Fecha inicio',false)!!}
										<div class="form-group">
											<div class="input-group date" id="fecha_filtro_inicio" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" data-target="#fecha_filtro_inicio" id="fecha_filtro_inicio1" name="fecha_filtro_inicio"/>
												<div class="input-group-append" data-target="#fecha_filtro_inicio" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="fa fa-calendar"></i></div>
												</div>
												<span id="fecha_filtro_inicio2" class="invalid-feedback" role="alert">

												</span>
											</div>
											
										</div>											
									</td>
									<td>
										{!! Form::label('filtro', 'Fecha fin',false)!!}
										<div class="form-group">
											<div class="input-group date" id="fecha_filtro_fin" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" data-target="#fecha_filtro_fin" id="fecha_filtro_fin1" name="fecha_filtro_fin"/>
												<div class="input-group-append" data-target="#fecha_filtro_fin" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="fa fa-calendar"></i></div>
												</div>
												<span id="fecha_filtro_fin2" class="invalid-feedback" role="alert">

												</span>	
											</div>
										</div>											
									</td>
								</tr>
							</tbody>
						</table>
						{!! link_to('', 'Mostrar todos', ['id'=>'todos', 'class'=>'btn btn-primary pull-left ml-3'], $secure=null) !!}
						{!! link_to('', 'Filtrar', ['id'=>'filtrar', 'class'=>'btn btn-success pull-right'], $secure=null) !!}
						{!! Form::submit('Volver', ['class' => 'btn btn-primary pull-right', 'onclick' => 'history.go(-1); return false;']) !!}
						{!! Form::close() !!}
					</div>

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-body">
									<div class="tab-content">
										<strong>Listado de uso de servicios</strong><br><br>
										<table class="table table-striped table-bordered table-sm" id="table_seguimiento"  style="width:100%">
											<thead>
												<tr class="bg-success">
													<th scope="col">Pvd</th>
													<th scope="col">Nombre servicio</th>
													<th scope="col">Fecha</th>
													<th scope="col">Hora inicio</th>
													<th scope="col">Hora fin</th>
													<th scope="col">Cantidad</th>
													<th scope="col">Tarifa</th>
													<th scope="col">Total</th>
												</tr>
											</thead>
										</table>
									</div> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

