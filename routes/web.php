<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/home', 'HomeController@index')->name('home');

//RESOURCE PARA USO EQUIPO
Route::resource('uso_equipo','Uso_equipoController');

//RUTA PARA GENERAR PDF USO EQUIPO
Route::get('pdf/{id}','Uso_equipoController@pdf');

//RUTA PARA GENERAR PDF RESERVAS
Route::get('pdf_reserva/{id}','ReservaController@pdf');

//RESOURCE PARA RESERVA
Route::resource('reserva','ReservaController');

//RESOURCE PARA SERVICIO
Route::resource('servicio','ServicioController');

//RESOURCE PARA CARGAR SALAS E INVENTARIO A UN PVD
Route::resource('sala','SalaController');
Route::post('inventario','SalaController@inventario')->name('inventario.store');

//RESOURCE PARA CARGAR MUNICIPIOS Y PVDS
Route::resource('mun_pvd','MunPvdController');

//RESOURCE PARA AGREGAR SERVICIO PVD
Route::resource('agregar_s_pvd','Agregar_servicio_pvdController');

//RESOURCE PARA EL SEGUIMIENTO DE USO EQUIPOS
Route::resource('seguimiento_uso','SeguimientoUsoController');
Route::get('servicios_pvd/{id}', 'SeguimientoUsoController@servicios_pvd');

//DEVULVE LOS PVDS
Route::get('pvd/{id}', 'Uso_equipoController@get_pvd');

//RUTA PARA SABER SI UN SERVICIO ES EQUIPO O NO
Route::get('es_equipo/{id}', 'Uso_equipoController@es_equipo');

//RUTAS PARA GENERAR REPORTES
Route::get('reporte_reserva_pvd','ReportesController@reporte_reserva_pvd')->name('reporte_reserva_pvd');
Route::get('reporte_uso_equipo_pvd','ReportesController@reporte_uso_equipo_pvd')->name('reporte_uso_equipo_pvd');
Route::get('reporte_reserva_total','ReportesController@reporte_reserva_total')->name('reporte_reserva_total');
Route::get('reporte_uso_equipo_total','ReportesController@reporte_uso_equipo_total')->name('reporte_uso_equipo_total');

//RESOURCE PARA INVENTARIOS
Route::resource('crear_inventario','CrearInventarioSalaController');


//RESOURCE PARA SALAS
Route::resource('crear_sala','CrearSalaController');

