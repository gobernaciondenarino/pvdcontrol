///DATATABLE DE SERVICIO
var table_servicio = $('#table_servicios').DataTable({
	language: {
		"emptyTable":	"No hay datos disponibles en la tabla.",
		"info":	"Del _START_ al _END_ de _TOTAL_ ",
		"infoEmpty": "Mostrando 0 registros de un total de 0.",
		"infoFiltered":	"(filtrados de un total de _MAX_ registros)",
		"infoPostFix":	"(actualizados)",
		"lengthMenu": "Mostrar _MENU_ registros",
		"loadingRecords": "Cargando...",
		"processing": "Procesando...",
		"search": "Buscar:",
		"searchPlaceholder":"Dato para buscar",
		"zeroRecords":	"No se han encontrado coincidencias.",
		"paginate": {
			"first":"Primera",
			"last":	"Última",
			"next":	"Siguiente",
			"previous":"Anterior"
		},
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: 'servicio/create',
		type: "GET"
	},
	columns:[
		{data: 'cod_servicio'},
		{data: 'nombre_servicio'},
		{data: 'equipo', name: 'equipo', orderable: false, searchable: false},
		{data: 'action', name: 'action', orderable: false, searchable: false}
	]
});

//GUARDA SERVICIO
var errores = null;
$("#guardar_servicio").click(function(e){
	e.preventDefault();

	var id = $(this).attr("id");
	var formData = new FormData(document.getElementById("form_crear_servicio"));

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	if(id=="guardar_servicio"){
		$.ajax({
			url: "servicio",
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			success: function(data){
				console.log(data);

				if(data.errors != null){
					errores = data;
					$.each( data.errors, function( key, val ) {
						$('#'+key).addClass('is-invalid');
						$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
					});		
				}else{
					table_servicio.ajax.reload();
					$('#form_crear_servicio')[0].reset();
					toastr.success('', 'Servicio creado');
				}
			}
		})	
	}else{
		$.ajax({
			url: "servicio/"+id,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			success: function(data){
				if(data.errors != null){
					errores = data;
					$.each( data.errors, function( key, val ) {
						$('#'+key).addClass('is-invalid');
						$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
					});		
				}else{
					table_servicio.ajax.reload();
					$("#"+id).attr("id","guardar_servicio");
					$("#guardar_servicio").text("Guardar");
					$('input[name=_method]').val('POST');
					$('#form_crear_servicio')[0].reset();
					toastr.success('', 'Servicio editado');

				}
			}
		})
	}
});

//EDITA SERVICIO

function btn_editar_servicio($id){
	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}
	$.ajax({
		type: "GET",
		url: 'servicio/'+$id+'/edit',
		dataType: "json",
		success: function(data){
			console.log(data);
			$('#cod_servicio').val(data.cod_servicio);
			$('#nombre_servicio').val(data.nombre_servicio);
			$('#equipo').val(data.equipo);

			$("#guardar_servicio").text("Actualizar");
			$("#guardar_servicio").attr("id",$id);
			
			$('input[name=_method]').val('PATCH')
		},
		error: function(){

		}
	})
}

//ELIMINAR SERVICIO

function btn_eliminar_servicio($id){
	bootbox.confirm({
		message: "¿Esta seguro de eliminar este servicio?",
		buttons:{
			confirm: {
				label: '<i class="fa fa-check"></i> Eliminar',
				className: 'btn-danger'
			},
			cancel:{
				label: '<i class="fa fa-times"></i> Cancel',
				className: 'btn-info'
			}
		},
		callback: function(result){
			if (result) {
				$.ajax({
					url: 'servicio/'+$id,
					type: 'POST',
					data: {_token:window.laravel.token, 
							_method: 'DELETE'}, 
					success: function(data){
						console.log(data);
						if(data.msg!=null){
							toastr.warning('', 'El servicio esta siendo utilizado');
						}else{
							table_servicio.ajax.reload();
							toastr.success('', 'Servicio eliminado');
						}						
					},
					error: function(data){
					}			
				})
			}
		}
	});
}

