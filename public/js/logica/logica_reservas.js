
$("#exampleRadios1").click(function () {	 
	$('#tarifa').removeAttr('disabled');
});

$("#exampleRadios2").click(function () {	 
	$('#tarifa').val('').attr('disabled', 'disabled');
});

var errores;
$("#guardar_reserva").click(function(e){
	e.preventDefault();

	var formData = new FormData(document.getElementById("form_reserva"));

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			if(key == 'facturable')
			{
				$('#'+key+'2').removeClass('is-invalid');
			}
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	$.ajax({
		url: "reserva",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		success: function(data){
			if(data.errors != null){
				errores = data;
				$.each( data.errors, function( key, val ) {
					if(key == 'facturable')
					{
						$('#'+key+'2').addClass('is-invalid');
					}
					$('#'+key).addClass('is-invalid');
					$('#'+key+'1').empty().append('<strong>'+val+'</strong>');
				});		
			}else if (data != 'no'){

				if (data.factura == 'si') {
					setTimeout('factura_reservas('+data.reserva.id+')', 1000);
				}

				table_reservas.ajax.reload();
				$('#form_reserva')[0].reset();
				toastr.success('', 'Sala reservada exitosamente');
			}else{
				toastr.error('', 'Esta sala ya esta reservada en los intervalos de fecha y hora indicados');
			}
		}
	});	
	
});

//FACTURA
function factura_reservas(data){
	window.location.href = `./pdf_reserva/${data}`;
	toastr.options.preventDuplicates = false;
	toastr.options.timeOut = 3000;
	toastr.success('', 'Generando factura');

}

/////LENGUAJE DATATABLES////////////////////////////////////////////////////
var lenguaje = {
	"emptyTable":	"No hay datos disponibles.",
	"info":	"Del _START_ al _END_ de _TOTAL_ ",
	"infoEmpty": "Mostrando 0 registros de un total de 0.",
	"infoFiltered":	"(filtrados de un total de _MAX_ registros)",
	"infoPostFix":	"(actualizados)",
	"lengthMenu": "Mostrar _MENU_ registros",
	"loadingRecords": "Cargando...",
	"processing": "Procesando...",
	"search": "Buscar:",
	"searchPlaceholder":"Dato para buscar",
	"zeroRecords":	"No se han encontrado coincidencias.",
	"paginate": {
		"first":"Primera",
		"last":	"Última",
		"next":	"Siguiente",
		"previous":"Anterior"
	},
};


var table_reservas = $('#table_reservas').DataTable({
	language: lenguaje,
	processing: true,
	serverSide: true,
	ajax: {
		url: 'reserva/create',
		type: "GET"
	},
	columns:[
		{data: 'sala.nombre'},
		{data: 'solicitante'},
		{data: 'fecha_inicio'},
		{data: 'fecha_fin'},
		{data: 'hora_inicio'},
		{data: 'hora_fin'},
		{data: 'facturable'},
		{data: 'tarifa'},
	]
});