
///////////////////MOSTRAR SERVICIOS EL PVD DEL USUARIO AUTH///////////////////////////////
var id_servicio;
var servicioElejido;
$("#servicio").change(event =>{
	id_servicio = event.target.value;

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}
	
	//LIMPIAR CAMPOS
	$('#cantidad').val('').empty().attr('disabled', 'disabled');
	$('#tiempo').empty().append('<option selected="selected" value="">Selecciona...</option>')
	.attr('disabled', 'disabled');
	$('#hora_inicio').datetimepicker('clear');
	$('#hora_fin').datetimepicker('clear');
	$('#tarifa').val('');

	$.get(`es_equipo/${id_servicio}`, function(response, dep){
		servicioElejido = response;
		if (response.equipo == 1) {
			$('#tiempo').removeAttr('disabled');
			for(var i = 15; i <=60;i=i*2) {
				var cuanto = "";

				if(i == 15){cuanto = "Quince minutos";}else if (i==30) {cuanto = "Treinta minutos";}
				else if (i==60) {cuanto = "Una hora";}
				$("#tiempo").append('<option value='+i+'>'+cuanto+'</option>');
			}
		}else{
			$('#cantidad').removeAttr('disabled');	
		}
	});
});

////////////////////////GUARDAR USO///////////////////////////////////////////////////////
var errores = null;
$("#guardar_uso").click(function(e){
	e.preventDefault();
	
	$('#hora_inicio').removeAttr('disabled');
	$('#hora_fin').removeAttr('disabled');
	var formData = new FormData(document.getElementById("form_uso_equipo"));

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	$.ajax({
		url: "uso_equipo",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		success: function(data){
			if(data.errors != null){
				errores = data;
				$.each( data.errors, function( key, val ) {
					$('#'+key).addClass('is-invalid');
					$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
				});							
				$('#hora_inicio').attr('disabled', 'disabled');
				$('#hora_fin').attr('disabled', 'disabled');
			}else if(data!='no'){
				if (data.factura == "si") {
					setTimeout('factura('+data.uso.id+')', 1000);
				}
				toastr.success('', 'Servicio facturado');
				$('#tiempo').empty().attr('disabled', 'disabled');
				$('#cantidad').val('').attr('disabled', 'disabled');
				$('#hora_inicio').attr('disabled', 'disabled');
				$('#hora_fin').attr('disabled', 'disabled');

				$('#form_uso_equipo')[0].reset();	
			}else{
				$('#hora_inicio').attr('disabled', 'disabled');
				$('#hora_fin').attr('disabled', 'disabled');
				toastr.error('', 'Este servicio esta siendo utilizado en ese intervalo de horas');						
			}
		}
	})	
});

//FACTURA
function factura(data){
	window.location.href = `./pdf/${data}`;
	toastr.options.preventDuplicates = false;
	toastr.options.timeOut = 3000;
	toastr.success('', 'Generando factura');

}

//////////////////////TIEMPO////////////////////////////////
$("#tiempo").change(event =>{
	var time = moment();
	$('#hora_inicio3').datetimepicker('date', time);
	
	var time_fin = time.add(event.target.value, 'minute');
	$('#hora_fin3').datetimepicker('date', time_fin);

	var costo = 0;
	var t = event.target.value;
	if(t == 15){costo = 1;}else if(t == 30){costo = 2;}
	else if(t == 60){costo = 4;}
	var total = costo*servicioElejido.pivot.tarifa;
	$('#tarifa').val(total);

});

////////////////MOSTRAR TARIFA DEL SERVICIO - CANTIDAD ////////////////////////////////////
	
$("#cantidad").change(event =>{
	var total = event.target.value*servicioElejido.pivot.tarifa;
	$('#tarifa').val(total);
});

