var errores = null;
$("#cargar_salas").click(function(e){
	e.preventDefault();
	
	var formData = new FormData(document.getElementById("form_cargar_salas"));

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	$.ajax({
		url: "sala",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		success: function(data){			
			if(data.errors != null){
				errores = data;
				$.each( data.errors, function( key, val ) {
					$('#'+key).addClass('is-invalid');
					$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
				});		
			}else{
				$('#form_cargar_salas')[0].reset();
				toastr.success('', 'Salas cargadas');

				$('#sala').empty().append('<option selected="selected" value="">Selecciona...</option>');

				data.forEach(element => {
					console.log(element);
					$("#sala").append(`<option value=${element.id}> ${element.nombre} </option>`);
				});
			}
		}
	})
});

var errores = null;
$("#cargar_inventario").click(function(e){
	e.preventDefault();
	
	var formData = new FormData(document.getElementById("form_cargar_inventario"));

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	$.ajax({
		url: "inventario",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		success: function(data){			
			if(data.errors != null){
				errores = data;
				$.each( data.errors, function( key, val ) {
					$('#'+key).addClass('is-invalid');
					$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
				});		
			}else{
				$('#form_cargar_inventario')[0].reset();
				toastr.success('', 'Inventario de sala cargada');
			}
		}
	})
});