///////////DATEPICKER 1///////////////////////////////////////
$('#fecha_filtro_inicio').datetimepicker({
	format: 'L',
	autoclose: true
});

///////////DATEPICKER 2///////////////////////////////////////
$('#fecha_filtro_fin').datetimepicker({
	format: 'L',
	autoclose: true
});

////////////TOAST/////////////////////////////////////////////
toastr.options = {
	"progressBar": true,
	"closeButton": true,
	"positionClass": "toast-bottom-right",
	"timeOut": "2000",
	"preventDuplicates": true,
};

/////LENGUAJE DATATABLES////////////////////////////////////////////////////
var lenguaje = {
	"emptyTable":	"No hay datos disponibles del filtro realizado.",
	"info":	"Del _START_ al _END_ de _TOTAL_ ",
	"infoEmpty": "Mostrando 0 registros de un total de 0.",
	"infoFiltered":	"(filtrados de un total de _MAX_ registros)",
	"infoPostFix":	"(actualizados)",
	"lengthMenu": "Mostrar _MENU_ registros",
	"loadingRecords": "Cargando...",
	"processing": "Procesando...",
	"search": "Buscar:",
	"searchPlaceholder":"Dato para buscar",
	"zeroRecords":	"No se han encontrado coincidencias.",
	"paginate": {
		"first":"Primera",
		"last":	"Última",
		"next":	"Siguiente",
		"previous":"Anterior"
	},
};

//TABLE SEGUIMIENTO ////////////////////////////////////////
$("#todos").click(function(e){
	e.preventDefault();

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key+"1").removeClass('is-invalid');			
			$('#'+key+"2").empty();
		});
	}
	
	toastr.success('', 'Todos los servicios utilizados');

	table_seguimiento.destroy();
	table_seguimiento = $('#table_seguimiento').DataTable({
		language: lenguaje,
		processing: true,
		serverSide: true,
		ajax: {
			url: 'seguimiento_uso/create',
			type: "GET"
		},
		columns:[
		{data: 'pvd', name: 'pvd'},
		{data: 'servicio', name: 'servicio'},
		{data: 'fecha'},
		{data: 'hora_inicio'},
		{data: 'hora_fin'},
		{data: 'cantidad'},
		{render: function ( data, type, row ) {
			var numero = row.tarifa;
			var a = numero.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
			return '$ '+a;
		} },
		{render: function ( data, type, row ) {
			var numero = row.total;
			var a = numero.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");			
			return '$ '+a;
		} },
		]
	});
});

var table_seguimiento = $('#table_seguimiento').DataTable({
	language: lenguaje,
	processing: true,
	serverSide: true,
	ajax: {
		url: 'seguimiento_uso/create',
		type: "GET"
	},
	columns:[
	{data: 'pvd'},
	{data: 'servicio'},
	{data: 'fecha'},
	{data: 'hora_inicio'},
	{data: 'hora_fin'},
	{data: 'cantidad'},
	{render: function ( data, type, row ) {
		var numero = row.tarifa;
		var a = numero.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
		return '$ '+a;
	} },
	{render: function ( data, type, row ) {
		var numero = row.total;
		var a = numero.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");			
		return '$ '+a;
	} },
	]
});

///ELIJE PVD
$("#pvd_id").change(event =>{
	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key+"1").removeClass('is-invalid');			
			$('#'+key+"2").empty();
		});
	}
	$.get(`servicios_pvd/${event.target.value}`, function(response, dep){
		if(response!=""){
			console.log(response);
			$('#servicio_id').empty().append('<option selected="selected" value="">Selecciona...</option>');

			response.forEach(element => {
				$("#servicio_id").append(`<option value=${element.id}> ${element.nombre_servicio} </option>`);
			});
		}else{
			$('#servicio_id').empty().append('<option selected="selected" value="">Selecciona...</option>');			
		}
	});	
});

//FILTRAR///////////////////////////////////////////////////
var errores = null;
$("#filtrar").click(function(e){
	e.preventDefault();
	
	var formData = new FormData(document.getElementById("form_seguimiento_uso"));

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key+"1").removeClass('is-invalid');			
			$('#'+key+"2").empty();
		});
	}

	$.ajax({
		url: "seguimiento_uso",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		success: function(data){
			console.log(data);
			
			if(data.errors != null){
				errores = data;
				$.each( data.errors, function( key, val ) {
					$('#'+key+"1").addClass('is-invalid');
					$('#'+key+"2").empty().append("<strong>"+val+"</strong>");
				});							
			}else if(data!='no' && data!='entradas' && data!= null){	
				toastr.success('', 'Filtro realizado');
				table_seguimiento.destroy();

				table_seguimiento = $('#table_seguimiento').DataTable( {
					language: lenguaje,
					data: data,
					columns: [
					{data : 'pvd.nombre'},
					{data : 'servicio.nombre_servicio'},
					{data : 'uso.fecha'},       
					{data : 'uso.hora_inicio'},          
					{data : 'uso.hora_fin'},          
					{data : 'uso.cantidad'},             
					{render: function ( data, type, row ) {
						var numero = row.uso.tarifa;
						var a = numero.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
						return '$ '+a;
					} },
					{render: function ( data, type, row ) {
						var numero = row.uso.total;
						var a = numero.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");			
						return '$ '+a;
					} }, 
					]
				});	

				$('#form_seguimiento_uso')[0].reset();				

			}else if(data=='no'){
				toastr.error('', 'No se encontraron resultados del filtro')
			}else{
				toastr.error('', 'Ingrese al menos un campo para hacer el filtro')
			}
		}
	})
});