//CAMBIOS AUTH - REGISTRAR
var err = null;

$("#auth_añadir").click(function(e){
	e.preventDefault();

	if (err!=null) {
		$.each( err.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	var formData = new FormData(document.getElementById("registrar"));

	$.ajax({
		url: "register",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,
		success: function(data){
			console.log(data);
			
			if(data.errors != null){
				err = data;
				$.each( data.errors, function( key, val ) {
					$('#'+key).addClass('is-invalid');
					$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
				});	
			}else{
				$('#registrar')[0].reset();				

				if(data.role=="admin_pvd"){
					toastr.success('', 'Administrador pvd registrado');
				}else{
					toastr.success('', 'Administrador global registrado');
				}
			}					
		}
	});	
});

