///DATATABLE DE SERVICIO
var table_servicio_pvd = $('#table_servicios_pvd').DataTable({
	language: {
		"emptyTable":	"No hay datos disponibles en la tabla.",
		"info":	"Del _START_ al _END_ de _TOTAL_ ",
		"infoEmpty": "Mostrando 0 registros de un total de 0.",
		"infoFiltered":	"(filtrados de un total de _MAX_ registros)",
		"infoPostFix":	"(actualizados)",
		"lengthMenu": "Mostrar _MENU_ registros",
		"loadingRecords": "Cargando...",
		"processing": "Procesando...",
		"search": "Buscar:",
		"searchPlaceholder":"Dato para buscar",
		"zeroRecords":	"No se han encontrado coincidencias.",
		"paginate": {
			"first":"Primera",
			"last":	"Última",
			"next":	"Siguiente",
			"previous":"Anterior"
		},
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: 'agregar_s_pvd/create',
		type: "GET"
	},
	columns:[

	{data: 'info', name: 'info', orderable: false, searchable: false},
	{data: 'info1', name: 'info1', orderable: false, searchable: false},
	{data: 'action', name: 'action', orderable: false, searchable: false}
	]
});

// GUARDA SERVICIO
var errores = null;
$("#agregar_servicio_pvd").click(function(e){
	e.preventDefault();

	var id = $(this).attr("id");
	
	$('#servicio_id').removeAttr('disabled');
	var formData = new FormData(document.getElementById("form_agregar_servicio_pvd"));

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	if(id=="agregar_servicio_pvd"){
		$.ajax({
			url: "agregar_s_pvd",
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			success: function(data){
				console.log(data);

				if(data.errors != null){
					errores = data;
					$.each( data.errors, function( key, val ) {
						$('#'+key).addClass('is-invalid');
						$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
					});		
				}else{
					table_servicio_pvd.ajax.reload();
					$('#form_agregar_servicio_pvd')[0].reset();
					toastr.success('', 'Servicio añadido al pvd');
				}
			}
		})	
	}else{		
		$.ajax({
			url: "agregar_s_pvd/"+id,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			success: function(data){
				console.log(data);

				if(data.errors != null){
					errores = data;
					$.each( data.errors, function( key, val ) {
						$('#'+key).addClass('is-invalid');
						$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
					});	
					$('#servicio_id').attr('disabled', 'disabled');
				}else{
					table_servicio_pvd.ajax.reload();
					$("#"+id).attr("id","agregar_servicio_pvd");
					$("#agregar_servicio_pvd").text("Agregar servicio");
					$('input[name=_method]').val('POST')
					$('#form_agregar_servicio_pvd')[0].reset();
					toastr.success('', 'Servicio del pvd editado');
				}
			}
		})
	}
});

//EDITA SERVICIO
function btn_editar_servicio_pvd($id){
	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	$('#servicio_id').attr('disabled', 'disabled');

	$.ajax({
		type: "GET",
		url: 'agregar_s_pvd/'+$id+'/edit',
		dataType: "json",
		success: function(data){
			console.log(data);
			$('#servicio_id').val(data.servicio_id);
			$('#tarifa').val(data.tarifa);

			$("#agregar_servicio_pvd").text("Actualizar");
			$("#agregar_servicio_pvd").attr("id",data.id);
			
			$('input[name=_method]').val('PATCH')
		},
		error: function(){

		}
	})
}

function btn_eliminar_servicio_pvd($id){
	bootbox.confirm({
		message: "¿Esta seguro de eliminar este servicio del PVD?",
		buttons:{
			confirm: {
				label: '<i class="fa fa-check"></i> Eliminar',
				className: 'btn-danger'
			},
			cancel:{
				label: '<i class="fa fa-times"></i> Cancel',
				className: 'btn-info'
			}
		},
		callback: function(result){
			if (result) {
				$.ajax({
					url: 'agregar_s_pvd/'+$id,
					type: 'POST',
					data: {_token:window.laravel.token, 
						_method: 'DELETE'}, 
						success: function(data){
							console.log(data);
							if(data.msg!=null){
								toastr.warning('', 'El servicio del pvd esta siendo utilizado');
							}else{
								table_servicio_pvd.ajax.reload();
								toastr.success('', 'Servicio eliminado del pvd');
							}
						},
						error: function(data){
							console.log(data);

						}			
					})
			}
		}
	});
}