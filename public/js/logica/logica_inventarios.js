/////LENGUAJE DATATABLES////////////////////////////////////////////////////
var lenguaje = {
	"emptyTable":	"No hay datos disponibles del filtro realizado.",
	"info":	"Del _START_ al _END_ de _TOTAL_ ",
	"infoEmpty": "Mostrando 0 registros de un total de 0.",
	"infoFiltered":	"(filtrados de un total de _MAX_ registros)",
	"infoPostFix":	"(actualizados)",
	"lengthMenu": "Mostrar _MENU_ registros",
	"loadingRecords": "Cargando...",
	"processing": "Procesando...",
	"search": "Buscar:",
	"searchPlaceholder":"Dato para buscar",
	"zeroRecords":	"No se han encontrado coincidencias.",
	"paginate": {
		"first":"Primera",
		"last":	"Última",
		"next":	"Siguiente",
		"previous":"Anterior"
	},
};

var table_inventario = $('#table_inventario').DataTable({
	language: lenguaje,
	processing: true,
	serverSide: true,
	ajax: {
		url: 'crear_inventario/create',
		type: "GET"
	},
	columns:[
		// {data: 'sala.nombre', name: 'sala.nombre'},
		// {data: 'nombre_elemento', name: 'nombre_elemento'},
		// {data: 'id', name: 'id'},
		// {data: 'cantidad_elem_mant_prev', name: 'cantidad_elem_mant_prev'},
		// {data: 'observaciones', name: 'observaciones'},
		{data: 'a', name: 'a'},
		{data: 'b', name: 'b'},
		{data: 'c', name: 'c'},
		{data: 'd', name: 'd'},
		{data: 'e', name: 'e'},
		{data: 'in', name: 'in'},
	]
});

// GUARDA O ACTUALIZA SALA PVD
var errores = null;
$("#crear_inventario").click(function(e){
	e.preventDefault();

	var id = $(this).attr("id");

	$('#sala_id').removeAttr('disabled');
	var formData = new FormData(document.getElementById("form_crear_inventario"));

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	if(id=="crear_inventario"){
		$.ajax({
			url: "crear_inventario",
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			success: function(data){
				console.log(data);

				if(data.errors != null){
					errores = data;
					$.each( data.errors, function( key, val ) {
						$('#'+key).addClass('is-invalid');
						$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
					});		
				}else{
					table_inventario.ajax.reload();
					$('#form_crear_inventario')[0].reset();
					toastr.success('', 'Inventario añadido a la sala');
				}
			}
		})	
	}else{		
		$.ajax({
			url: "crear_inventario/"+id,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			success: function(data){
				console.log(data);

				if(data.errors != null){
					errores = data;
					$.each( data.errors, function( key, val ) {
						$('#'+key).addClass('is-invalid');
						$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
					});	
					$('#sala_id').attr('disabled', 'disabled');

				}else{
					table_inventario.ajax.reload();
					$("#"+id).attr("id","crear_inventario");
					$("#crear_inventario").text("Agregar inventario");
					$('input[name=_method]').val('POST');
					$('#form_crear_inventario')[0].reset();
					toastr.success('', 'Inventario de la sala editado');
				}
			}
		})
	}
});

//VISTA EDITA SALA
function btn_editar_inventario_sala($id){

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	$('#sala_id').attr('disabled', 'disabled');	

	$.ajax({
		type: "GET",
		url: 'crear_inventario/'+$id+'/edit',
		dataType: "json",
		success: function(data){
			console.log(data);
			$('#sala_id').val(data.sala_id);
			$('#nombre_elemento').val(data.nombre_elemento);
			$('#unidad').val(data.unidad);
			$('#cantidad_inv').val(data.cantidad);
			$('#cantidad_elem_mant_prev').val(data.cantidad_elem_mant_prev);
			$('#observaciones').val(data.observaciones);
			$('#sala_id').val(data.sala_id);
			$('#nom_elemento').val(data.nom_elemento);
			$('#tipo_equipo').val(data.inv_detalle_sala.tipo_equipo);
			$('#marca').val(data.inv_detalle_sala.marca);
			$('#serial').val(data.inv_detalle_sala.serial);
			$('#placa').val(data.inv_detalle_sala.placa);
			$('#valor').val(data.inv_detalle_sala.valor);
			$('#valor_iva').val(data.inv_detalle_sala.valor_iva);

			$("#crear_inventario").text("Actualizar");
			$("#crear_inventario").attr("id",data.id);
			
			$('input[name=_method]').val('PATCH')
		},
		error: function(){

		}
	})
}

// ELIMINA SALA
function btn_eliminar_inventario_sala($id){

	bootbox.confirm({
		message: "¿Esta seguro de eliminar este inventario de la sala?",
		buttons:{
			confirm: {
				label: '<i class="fa fa-check"></i> Eliminar',
				className: 'btn-danger'
			},
			cancel:{
				label: '<i class="fa fa-times"></i> Cancelar',
				className: 'btn-info'
			}
		},
		callback: function(result){
			if (result) {
				$.ajax({
					url: 'crear_inventario/'+$id,
					type: 'POST',
					data: {_token:window.laravel.token, 
						_method: 'DELETE'}, 
						success: function(data){
							table_inventario.ajax.reload();
							toastr.success('', 'Inventario eliminado de la sala');																						
						},
						error: function(data){

						}			
					})
			}
		}
	});
}