/////LENGUAJE DATATABLES////////////////////////////////////////////////////
var lenguaje = {
	"emptyTable":	"No hay datos disponibles del filtro realizado.",
	"info":	"Del _START_ al _END_ de _TOTAL_ ",
	"infoEmpty": "Mostrando 0 registros de un total de 0.",
	"infoFiltered":	"(filtrados de un total de _MAX_ registros)",
	"infoPostFix":	"(actualizados)",
	"lengthMenu": "Mostrar _MENU_ registros",
	"loadingRecords": "Cargando...",
	"processing": "Procesando...",
	"search": "Buscar:",
	"searchPlaceholder":"Dato para buscar",
	"zeroRecords":	"No se han encontrado coincidencias.",
	"paginate": {
		"first":"Primera",
		"last":	"Última",
		"next":	"Siguiente",
		"previous":"Anterior"
	},
};

var table_salas_pvd = $('#table_salas_pvd').DataTable({
	language: lenguaje,
	processing: true,
	serverSide: true,
	ajax: {
		url: 'crear_sala/create',
		type: "GET"
	},
	columns:[
		{data: 'nombre'},
		{data: 'action1', name: 'action1'}
	]
});

// GUARDA O ACTUALIZA SALA PVD
var errores = null;
$("#agregar_sala").click(function(e){
	e.preventDefault();

	var id = $(this).attr("id");
	
	var formData = new FormData(document.getElementById("form_agregar_sala"));

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	if(id=="agregar_sala"){
		$.ajax({
			url: "crear_sala",
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			success: function(data){

				if(data.errors != null){
					errores = data;
					$.each( data.errors, function( key, val ) {
						$('#'+key).addClass('is-invalid');
						$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
					});		
				}else{
					table_salas_pvd.ajax.reload();
					$('#form_agregar_sala')[0].reset();
					toastr.success('', 'Sala añadida al pvd');
				}
			}
		})	
	}else{		
		$.ajax({
			url: "crear_sala/"+id,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			success: function(data){

				if(data.errors != null){
					errores = data;
					$.each( data.errors, function( key, val ) {
						$('#'+key).addClass('is-invalid');
						$('#'+key+"1").empty().append("<strong>"+val+"</strong>");
					});	
				}else{
					table_salas_pvd.ajax.reload();
					$("#"+id).attr("id","agregar_sala");
					$("#agregar_sala").text("Agregar sala");
					$('input[name=_method]').val('POST');
					$('#form_agregar_sala')[0].reset();
					toastr.success('', 'Sala del pvd editada');
				}
			}
		})
	}
});

//VISTA EDITA SALA
function btn_editar_sala($id){
	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+"1").empty();
		});
	}

	$.ajax({
		type: "GET",
		url: 'crear_sala/'+$id+'/edit',
		dataType: "json",
		success: function(data){
			$('#nombre').val(data.nombre);

			$("#agregar_sala").text("Actualizar");
			$("#agregar_sala").attr("id",data.id);
			
			$('input[name=_method]').val('PATCH')
		},
		error: function(){

		}
	})
}

// ELIMINA SALA
function btn_eliminar_sala($id){
	bootbox.confirm({
		message: "¿Esta seguro de eliminar esta sala del PVD?",
		buttons:{
			confirm: {
				label: '<i class="fa fa-check"></i> Eliminar',
				className: 'btn-danger'
			},
			cancel:{
				label: '<i class="fa fa-times"></i> Cancelar',
				className: 'btn-info'
			}
		},
		callback: function(result){
			if (result) {
				$.ajax({
					url: 'crear_sala/'+$id,
					type: 'POST',
					data: {_token:window.laravel.token, 
						_method: 'DELETE'}, 
						success: function(data){
							if (data == 'ocupada') {
								toastr.warning('', 'La sala esta siendo utilizada');							

							}else if (data == 'inv') {
								toastr.warning('', 'La sala tiene un inventario añadido');															
							}
							else{
								table_salas_pvd.ajax.reload();
								toastr.success('', 'Sala eliminada del pvd');		
							}																
						},
						error: function(data){

						}			
					})
			}
		}
	});
}