var errores = null;
$('#cargar_mun_pvd').click(function(e){
	e.preventDefault();
	
	var formData = new FormData(document.getElementById('form_cargar_mun_pvd'));

	if (errores!=null) {
		$.each( errores.errors, function( key, val ) {
			$('#'+key).removeClass('is-invalid');			
			$('#'+key+'1').empty();
		});
	}

	$.ajax({
		url: 'mun_pvd',
		type: 'POST',
		data: formData,
		contentType: false,
		processData: false,
		success: function(data){			
			if(data.errors != null){
				errores = data;
				$.each( data.errors, function( key, val ) {
					$('#'+key).addClass('is-invalid');
					$('#'+key+'1').empty().append('<strong>'+val+'</strong>');
				});		
			}else{
				$('#form_cargar_mun_pvd')[0].reset();
				toastr.success('', 'Municipios y pvds cargados');				
			}
		}
	})
});
