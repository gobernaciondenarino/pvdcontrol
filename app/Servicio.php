<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $fillable = [
        'id', 'cod_servicio', 'nombre_servicio', 'equipo',
    ];

    public $timestamps = false;
    	
	//Un servicio tiene muchos pvds
 	public function pvds()
	{
	   return $this->belongsToMany('App\Pvd', 'pvd_servicios');
	}

}
