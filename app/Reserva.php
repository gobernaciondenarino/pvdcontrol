<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    public $timestamps = false;
	
    protected $fillable = [
        'solicitante', 'facturable', 'estado', 'fecha_inicio', 'fecha_fin',
        'hora_inicio', 'hora_fin', 'tarifa', 'sala_id',
    ];

    //Una reserva pertenece a una sala
    public function sala()
    {
       return $this->belongsTo('App\Sala');
    }
}
