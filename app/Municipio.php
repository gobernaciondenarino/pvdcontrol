<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'id', 'dane', 'nombre',
    ];

    public function pvd()
    {
        return $this->hasOne('App\Pvd');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }
}
