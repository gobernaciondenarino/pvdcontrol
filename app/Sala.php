<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sala extends Model
{
    
    protected $fillable = [
        'id', 'nombre', 'pvd_id',
    ];
    
    public $timestamps = false;

    //Una sala pertenece a un pvd 
    public function pvd()
	{
	   return $this->belongsTo('App\Pvd');
	}

    //Una sala tiene un inventario
    public function inv_sala()
    {
        return $this->hasOne('App\Inv_sala')->with('inv_detalle_sala');
    }

    //Una sala tiene muchas reservas
    public function reservas()
    {
        return $this->hasMany('App\Reserva')->orderBy('fecha_inicio')->orderBy('hora_inicio');
    }
}
