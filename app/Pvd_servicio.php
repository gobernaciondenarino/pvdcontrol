<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pvd_servicio extends Model
{
    protected $fillable = [
        'id', 'tarifa', 'pvd_id', 'servicio_id',
    ];

    public $timestamps = false;

    //Un pvd_servicio tiene muchos uso equipo
    public function uso_equipos()
    {
       return $this->hasMany('App\Uso_equipo')->orderBy('fecha')->orderBy('hora_inicio');
    }

    public function pvd()
    {
       return $this->belongsTo('App\Pvd');
    }

    public function servicio()
    {
       return $this->belongsTo('App\Servicio');
    }
}
