<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uso_equipo extends Model
{
    protected $fillable = [
        'id', 'cantidad', 'fecha', 'hora_inicio', 'hora_fin', 'solicitante', 'tarifa', 'total', 'pvd_servicio_id',
    ];

    public $timestamps = false;

    // Un uso equipo pertenece un servicio
	public function pvd_servicio()
    {
        return $this->belongsTo('App\Pvd_servicio');
    }
}
