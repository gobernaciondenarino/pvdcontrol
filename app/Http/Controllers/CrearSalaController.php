<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Municipio;
use App\Pvd;
use App\Sala;

class CrearSalaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('atras');
        $this->middleware('roleAdminPvd');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $select_municipio = Municipio::where('id', '=', Auth::user()->municipio->id)->pluck('nombre', 'id');
        $select_pvd = Pvd::where('municipio_id', '=', Auth::user()->municipio->id)->pluck('nombre', 'id');
        

        return view('layouts.salas.salas', compact('select_municipio', 'select_pvd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pvd = Auth::user()->municipio->pvd;
        $salas = Sala::where('pvd_id', $pvd->id)->get();

        return datatables()
            ->eloquent(Sala::where('pvd_id', $pvd->id))
            ->addColumn('action1', function($salas){
                return 
                '<div class="row">'.
                '<div class="col text-center">'.
                    '<a href="#" onclick="btn_editar_sala('. $salas->id .')" class="btn btn-primary btn-sm editar" ><i class="nav-icon fa fa-edit"></i></a>'.
                    '<a href="#" onclick="btn_eliminar_sala('. $salas->id .')" class="btn btn-danger btn-sm eliminar" ><i class="nav-icon fa fa-trash"></i></a>'.
                '</div>'.
                '</div>';
            })  
            ->rawColumns(['action1'])            
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'nombre' => 'required',                                                        
        ]);

        $pvd = Auth::user()->municipio->pvd;
        $data['pvd_id'] = $pvd->id;

        Sala::create($data);
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sala = Sala::find($id);
        return response()->json($sala);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sala = Sala::find($id);    

        $data = $request->validate([
            'nombre' => 'required',
        ]);

        $sala->update($data);

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sala = Sala::find($id); 

        if (count($sala->reservas)) {
            return response()->json('ocupada');            
        }else if ($sala->inv_sala != null) {
            return response()->json('inv');            
        }

        $sala->delete();
        return response()->json($sala);
    }
}
