<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Reserva;
use App\Municipio;
use App\Pvd;
use App\Sala;

class ReservaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('atras');
        $this->middleware('roleAdminPvd');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $mun = Auth::user()->municipio;

        $select_municipio = Municipio::where('id', $mun->id)->pluck('nombre', 'id');        
        $select_pvd = Pvd::where('id', $mun->pvd->id)->pluck('nombre', 'id');
        $select_salas = Sala::where('pvd_id', $mun->pvd->id)->pluck('nombre', 'id');

        return view('layouts.reserva', compact('select_municipio', 'select_pvd', 'select_salas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pvd = Auth::user()->municipio->pvd->id;

        $reservas = Reserva::whereHas('sala', function($q) use($pvd) {
            $q->where('pvd_id', $pvd);
        })->with('sala')->get();


        return datatables()
        ->eloquent(Reserva::whereHas('sala', function($q) use($pvd) {
            $q->where('pvd_id', $pvd);
        })->with('sala'))
        ->addColumn('facturable', function($reservas){
            if ($reservas->facturable == 0) {
                return 'No facturable';                            
            }
            return 'Facturable';
        })
        ->addColumn('tarifa', function($reservas){
            if ($reservas->tarifa == null) {
                return '0';                
            }
            return $reservas->tarifa;
        })  
        ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'facturable' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required|after_or_equal:fecha_inicio',
            'hora_inicio' => 'required',
            'hora_fin' => 'required|after:hora_inicio',
            'sala_id' => 'required'                           
        ]);
        
        $data['fecha_inicio'] = Carbon::parse($data['fecha_inicio'])->toDateString();
        $data['fecha_fin'] = Carbon::parse($data['fecha_fin'])->toDateString();
        $hora_inicio = Carbon::parse($data['hora_inicio']);
        $hora_fin = Carbon::parse($data['hora_fin']);

        $reservas = Reserva::where(function($query) use($data){
                        $query->whereBetween('fecha_inicio', [$data['fecha_inicio'], $data['fecha_fin']])
                            ->orWhereBetween('fecha_fin', [$data['fecha_inicio'], $data['fecha_fin']])
                            ->orWhereRaw('? between fecha_inicio and fecha_fin', [$data['fecha_inicio']])
                            ->orWhereRaw('? between fecha_inicio and fecha_fin', [$data['fecha_fin']]);
                    })->where('sala_id', '=', $data['sala_id'])->get();

        foreach ($reservas as $reserva) {
            $hinicio = Carbon::parse($reserva->hora_inicio);
            $hfin = Carbon::parse($reserva->hora_fin);
            if ($hora_inicio->between($hinicio, $hfin) || $hora_fin->between($hinicio, $hfin)
                || $hinicio->between($hora_inicio, $hora_fin) || $hfin->between($hora_inicio, $hora_fin) ) {
                return response()->json('no');  
            }
        }

        $data['hora_inicio'] = $hora_inicio->toTimeString();
        $data['hora_fin'] = $hora_fin->toTimeString();

        if ($data['facturable'] == '1') {
            $t = $request->validate([
                'tarifa' => 'required',
            ]);

            $data['tarifa'] = $t['tarifa'];
            $data['solicitante'] = 'Cliente defecto';
        }else{
            $data['solicitante'] = 'Cliente estrella';
        }

        if ($request->factura == 'si') {
            $data['factura'] = $request->factura;
        }

        $data['estado'] = '1'; 

        $reserva = Reserva::create($data);  
        $data['reserva'] = $reserva;

        return response()->json($data); 
    }

    public function pdf($id)
    {   
        $reserva = Reserva::find($id);
        $factual = Carbon::parse('America/Bogota');
        $factual = $factual->format('Y'.'/'.'m'.'/'.'d');

        $pdf = new \PDF;
        $view =  \View::make('layouts.pdf.generar_pdf_reserva', compact('reserva', 'factual'))->render();
        $pdf = \PDF::loadHTML($view);
        $pdf->setPaper('A4', 'landscape');

        return $pdf->stream();           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
