<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\Pvd;
use App\Servicio;
use App\Pvd_servicio;
use App\Uso_equipo;

class SeguimientoUsoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('atras');
        $this->middleware('roleAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pvds = Pvd::where('nombre', '!=', 'Pvd global')->pluck('nombre', 'id');
        $servicios = Servicio::pluck('nombre_servicio', 'id');

        return view('layouts.seguimiento.uso', compact('pvds', 'servicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usos = Uso_equipo::all();
        
        return datatables()
        ->eloquent(Uso_equipo::query())
        ->addColumn('pvd', function($usos){
            $pvd_serv = Pvd_servicio::all();
            foreach ($pvd_serv as $servicio) {
                if($usos->pvd_servicio_id == $servicio->id){
                    $pvd = Pvd::find($servicio->pvd_id);                            
                    break;
                } 
            }
            return $pvd->nombre;
        })
        ->addColumn('servicio', function($usos){
            $pvd_serv = Pvd_servicio::all();
            foreach ($pvd_serv as $servicio) {
                if($usos->pvd_servicio_id == $servicio->id){
                    $serv = Servicio::find($servicio->servicio_id);                            
                    break;
                } 
            }
            return $serv->nombre_servicio;
        })  
        ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $pvd = Pvd::find($request->pvd_id);
        $servicio_request = $request->servicio_id;
        $fecha_inicio_request = $request->fecha_filtro_inicio;
        $fecha_fin_request = $request->fecha_filtro_fin;

        if ($pvd == null && $servicio_request ==null &&
            $fecha_inicio_request == null && $fecha_fin_request == null) {
            return response()->json('entradas');            
        }else{
            if($fecha_inicio_request != null){
                $fecha_inicio_request = Carbon::parse($fecha_inicio_request);            
                $data = $request->validate([
                    'fecha_filtro_fin' => 'required|after_or_equal:'.$request->fecha_filtro_inicio,  
                ]);
                $fecha_fin_request = $data['fecha_filtro_fin'];
            }

            if($fecha_fin_request != null){
                $fecha_fin_request = Carbon::parse($fecha_fin_request);            
                $data = $request->validate([
                    'fecha_filtro_inicio' => 'required',                           
                ]);
            }

            $usos= Uso_equipo::all();        
            $pvd_serv = Pvd_servicio::all();

            $uso_equipo = [];

            if($pvd!=null){
                if($servicio_request!=null){
                    if ($fecha_inicio_request != null && $fecha_fin_request != null) {
                        //PVD-SERVICIO-FECHA
                        $pvd_servicio = Pvd_servicio::where('servicio_id', '=', $servicio_request)->where('pvd_id', '=', $pvd->id)->get();
                        if ($pvd_servicio!=null) {
                            $uso = Uso_equipo::where('pvd_servicio_id', '=', $pvd_servicio[0]->id)
                            ->whereBetween('fecha', [$fecha_inicio_request, $fecha_fin_request])->get();
                            $serv = Servicio::find($pvd_servicio[0]->servicio_id);                        
                            foreach ($uso as $u) {
                                $uso_equipo[]= array('uso' => $u, 'servicio' =>  $serv, 'pvd' => $pvd );                                
                            }
                        }

                    }else{
                        // PVD-SERVICIO
                        $pvd_servicio = Pvd_servicio::where('servicio_id', '=', $servicio_request)->where('pvd_id', '=', $pvd->id)->get();
                        if ($pvd_servicio!=null) {
                            $uso = Uso_equipo::where('pvd_servicio_id', '=', $pvd_servicio[0]->id)->get();                            
                            $serv = Servicio::find($pvd_servicio[0]->servicio_id);                        
                            foreach ($uso as $u) {
                                $uso_equipo[]= array('uso' => $u, 'servicio' =>  $serv, 'pvd' => $pvd );                                
                            } 
                        }
                    } 

                }else if ($fecha_inicio_request != null && $fecha_fin_request != null) {
                    // PVD-FECHA
                    $pvd_servicio = Pvd_servicio::where('pvd_id', '=', $pvd->id)->get();

                    if ($pvd_servicio!=null) {
                        foreach ($pvd_servicio as $p) {
                            $serv = Servicio::find($p->servicio_id);                        
                            $uso = Uso_equipo::where('pvd_servicio_id', '=', $p->id)
                            ->whereBetween('fecha', [$fecha_inicio_request, $fecha_fin_request])->get();                            
                            foreach ($uso as $u) {
                                $uso_equipo[]= array('uso' => $u, 'servicio' =>  $serv, 'pvd' => $pvd );                                
                            } 

                        }
                    }                    
                }else{
                    // PVD
                    $pvd_servicio = Pvd_servicio::where('pvd_id', '=', $pvd->id)->get();
                    if ($pvd_servicio!=null) {
                        foreach ($pvd_servicio as $p) {
                            $serv = Servicio::find($p->servicio_id);                        
                            $uso = Uso_equipo::where('pvd_servicio_id', '=', $p->id)->get();                            
                            foreach ($uso as $u) {
                                $uso_equipo[]= array('uso' => $u, 'servicio' =>  $serv, 'pvd' => $pvd );                                
                            }   

                        }
                    }
                }
            }else if($request->servicio_id!=null){            
                if ($fecha_inicio_request != null || $fecha_fin_request !=null) {
                    // SERVICIO-FECHA
                    $pvd_servicio = Pvd_servicio::where('servicio_id', '=', $servicio_request)->get();                   
                    foreach ($pvd_servicio as $p) {
                        $uso = Uso_equipo::where('pvd_servicio_id', '=', $p->id)
                                            ->whereBetween('fecha', [$fecha_inicio_request, $fecha_fin_request])->get();
                        $serv = Servicio::find($p->servicio_id);   
                        $pvd = Pvd::find($p->pvd_id);                     
                        foreach ($uso as $u) {
                            $uso_equipo[]= array('uso' => $u, 'servicio' =>  $serv, 'pvd' => $pvd );                                
                        }
                    } 
                }else{
                    $pvd_servicio = Pvd_servicio::where('servicio_id', '=', $servicio_request)->get();
                    foreach ($pvd_servicio as $p) {
                        $uso = Uso_equipo::where('pvd_servicio_id', '=', $p->id)->get();
                        $serv = Servicio::find($p->servicio_id);
                        $pvd = Pvd::find($p->pvd_id);                     
                        foreach ($uso as $u) {
                            $uso_equipo[]= array('uso' => $u, 'servicio' =>  $serv, 'pvd' => $pvd );                                
                        }
                    }
                }            
            }else if($fecha_inicio_request != null || $fecha_fin_request !=null) {

                $uso = Uso_equipo::whereBetween('fecha', [$fecha_inicio_request, $fecha_fin_request])->get();
                foreach ($uso as $u) {
                    $serv = Servicio::find($u->pvd_servicio->servicio_id);   
                    $pvd = Pvd::find($u->pvd_servicio->pvd_id);                     

                    $uso_equipo[]= array('uso' => $u, 'servicio' =>  $serv, 'pvd' => $pvd );                                
                }
            }
        }

        if(empty($uso_equipo)){
            return "no";
        }
        return response()->json($uso_equipo);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function servicios_pvd(Request $request, $id){
        if($request->ajax()){
            $pvd = Pvd::find($id);
            return response()->json($pvd->servicios);
        }
    }
}
