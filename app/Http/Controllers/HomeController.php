<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Uso_equipo;
use App\Reserva;
use App\Pvd_servicio;
use App\Pvd;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('atras');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pvd = Auth::user()->municipio->pvd;

        $usos = Uso_equipo::whereHas('pvd_servicio', function($q) use($pvd) {
            $q->where('pvd_id', $pvd->id);
        })->get();

        $reservas = Reserva::whereHas('sala', function($q) use($pvd) {
            $q->where('pvd_id', $pvd->id);
        })->get();

        $pvd_servicios = Pvd_servicio::orderBy('pvd_id')->get();

        $pvds = Pvd::where('nombre', '!=', 'Pvd global')->get();


        return view('layouts.home', compact('usos', 'reservas', 'pvd_servicios', 'pvds'));
    }
}
