<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicio;

class ServicioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('atras');
        $this->middleware('roleAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.servicio.crear_servicio');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $servicios = Servicio::all();

        return datatables()
        ->eloquent(Servicio::query())
        ->addColumn('equipo', function($servicios){
            $data = ($servicios->equipo == true)?'Si':'No';
            return $data;
        })
        ->addColumn('action', function($servicios){
            return
            '<div class="row">'.
            '<div class="col text-center">'. 
            '<a href="#" onclick="btn_editar_servicio('. $servicios->id .')" class="btn btn-primary btn-sm editar" ><i class="nav-icon fa fa-edit"></i></a>'.
            '<a href="#" onclick="btn_eliminar_servicio('. $servicios->id .')" class="btn btn-danger btn-sm eliminar" ><i class="nav-icon fa fa-trash"></i></a>'.
            '</div>'.
            '</div>';
        })  
        ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'cod_servicio' => 'required|unique:servicios',
            'nombre_servicio' => 'required',                            
            'equipo' => 'required',                            
        ]);

        Servicio::create($data);
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = Servicio::find($id);
        return $servicio;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $servicio = Servicio::find($id);    

        $data = $request->validate([
            'cod_servicio' => 'required|unique:servicios,cod_servicio,'.$servicio->id,
            'nombre_servicio' => 'required',                            
            'equipo' => 'required',                            
        ]);

        $servicio->update($data);

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $servicio = Servicio::find($id);

     $count=0;
     $count+=count($servicio->pvds);
     if($count>0){
         return ['msg'=>'Elemento en uso', 'n' => $count];
     }

     $servicio->delete();
     return response()->json($servicio);
 }
}
