<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Municipio;
use App\Pvd;


class MunPvdController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('atras');
        $this->middleware('roleAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.cargar_mun_pvd.cargar_mun_pvd');        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'mun_pvd' => 'required|mimes:xls,xlsx',                         
        ]);


        $pvd = Auth::user()->municipio->pvd->id;

        $file = $data['mun_pvd'];
        $file_name = $file->getClientOriginalName();
        $file->move('files', $file_name);

        Excel::load('files/'.$file_name, function($reader) use($pvd){
            foreach ($reader->get() as $r) {
                $data=['dane' => $r->dane, 'nombre' => $r->nombre];    
                $municipio = Municipio::create($data);

                $data2=['codigo' => $r->codigo_pvd, 'nombre' => 'Pvd '.$municipio->nombre, 
                        'direccion' => $r->direccion, 'celular' => $r->celular,
                        'correo' => $r->correo, 'municipio_id' => $municipio->id,
                        ];   
                $pvd = Pvd::create($data2); 
            }
        });

        return response()->json('si'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
