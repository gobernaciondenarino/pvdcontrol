<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Municipio;
use App\Pvd;
use App\Inv_sala;
use App\Inv_detalle_sala;

class CrearInventarioSalaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('atras');
        $this->middleware('roleAdminPvd');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pvd = Auth::user()->municipio->pvd;
        $select_salas = $pvd->salas->pluck('nombre', 'id');
        $select_municipio = Municipio::where('id', '=', Auth::user()->municipio->id)->pluck('nombre', 'id');
        $select_pvd = Pvd::where('municipio_id', '=', Auth::user()->municipio->id)->pluck('nombre', 'id');

        return view('layouts.inventarios.inventarios', compact('select_salas', 'select_municipio', 'select_pvd'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pvd = Auth::user()->municipio->pvd->id;
        
        $inventarios = Inv_sala::whereHas('sala', function($q) use($pvd) {
            $q->where('pvd_id', $pvd);
        })->with('sala')->get();

        return datatables()
            ->eloquent(Inv_sala::whereHas('sala', function($q) use($pvd) {
                $q->where('pvd_id', $pvd);
            })->with('sala'))
            ->addColumn('a', function($inventarios){
                return $inventarios->sala->nombre;
            }) 
            ->addColumn('b', function($inventarios){
                return $inventarios->nombre_elemento;
            }) 
            ->addColumn('c', function($inventarios){
                return $inventarios->cantidad;
            }) 
            ->addColumn('d', function($inventarios){
                return $inventarios->cantidad_elem_mant_prev;
            })
            ->addColumn('e', function($inventarios){
                return $inventarios->observaciones;
            }) 
            ->addColumn('in', function($inventarios){
                return 
                '<div class="row">'.
                '<div class="col text-center">'.
                    '<a href="#" onclick="btn_editar_inventario_sala('.$inventarios->id.')" class="btn btn-primary btn-sm" ><i class="nav-icon fa fa-edit"></i></a>'.
                    '<a href="#" onclick="btn_eliminar_inventario_sala('.$inventarios->id .')" class="btn btn-danger btn-sm" ><i class="nav-icon fa fa-trash"></i></a>'.
                '</div>'.
                '</div>';
            })  
            ->rawColumns(['a', 'b', 'c', 'd', 'e', 'in'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data1 = $request->validate([
            'nombre_elemento' => 'required',                                                        
            'unidad' => 'required',                                                        
            'cantidad_inv' => 'required',                                                        
            'cantidad_elem_mant_prev' => 'required',                                                        
            'observaciones' => 'required',                                                        
            'sala_id' => 'required',                                                        
        ]);
        $data1['cantidad'] = $data1['cantidad_inv'];

        $data2 = $request->validate([
            'nombre_elemento' => 'required',                                                         
            'tipo_equipo' => 'required',                                                         
            'marca' => 'required',                                                         
            'serial' => 'required',                                                         
            'placa' => 'required',                                                         
            'valor' => 'required',                                                         
            'valor_iva' => 'required',                                                    
        ]);
        $data2['nom_elemento'] = $data2['nombre_elemento'];
        
        $inventario_sala = Inv_sala::create($data1);
        $data2['inv_sala_id'] = $inventario_sala->id;
        
        $inventario_detalle_sala = Inv_detalle_sala::create($data2);

        return response()->json('bien');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inventario_sala = Inv_sala::where('id', $id)->with('inv_detalle_sala')->get();
        return response()->json($inventario_sala[0]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inventario = Inv_sala::find($id);

        $data1 = $request->validate([
            'nombre_elemento' => 'required',                                                        
            'unidad' => 'required',                                                        
            'cantidad_inv' => 'required',                                                        
            'cantidad_elem_mant_prev' => 'required',                                                        
            'observaciones' => 'required',                                                        
            'sala_id' => 'required',                                                        
        ]);

        $data1['cantidad'] = $data1['cantidad_inv'];

        $data2 = $request->validate([
            'nombre_elemento' => 'required',                                                         
            'tipo_equipo' => 'required',                                                         
            'marca' => 'required',                                                         
            'serial' => 'required',                                                         
            'placa' => 'required',                                                         
            'valor' => 'required',                                                         
            'valor_iva' => 'required',                                   
        ]);
        $data2['nom_elemento'] = $data2['nombre_elemento'];

        $inventario->update($data1);
        $inventario_detalle = $inventario->inv_detalle_sala;
        
        $inventario_detalle->update($data2);

        return response()->json('bien');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inventario = Inv_sala::find($id);
        $inventario->inv_detalle_sala->delete();
        $inventario->delete();
        return response()->json('bien');
    }
}
