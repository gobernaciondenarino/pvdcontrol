<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Municipio;
use App\Pvd;
use App\Servicio;
use App\Pvd_servicio;
use App\Uso_equipo;


class Uso_equipoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('atras');
        $this->middleware('roleAdminPvd');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mun = Auth::user()->municipio;
        
        $select_municipio = Municipio::where('id', $mun->id)->pluck('nombre', 'id');
        $select_pvd = Pvd::where('id', $mun->pvd->id)->pluck('nombre', 'id');
        $select_servicio = $mun->pvd->servicios->pluck('nombre_servicio', 'id');
        return view('layouts.uso_equipo', compact('select_municipio', 'select_pvd','select_servicio'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $pvd = Auth::user()->municipio->pvd->id;

        $data1 = $request->validate([
            'servicio' => 'required',                                
        ]);

        $pvd_servicio = Pvd_servicio::where('pvd_id', '=', $pvd)->where('servicio_id', '=', $data1['servicio'])->first();
        $servicio_pvd = $pvd_servicio->servicio;        

        $date = Carbon::parse('America/Bogota');

        if ($servicio_pvd->equipo) {
            $data = $request->validate([
                'servicio' => 'required',
                'tiempo' => 'required',                            
                'tarifa' => 'required|Integer',                            
            ]);

            $cantidad = '';
            if($data['tiempo']=='15'){                    
                $cantidad = 'Quince minutos';
            }
            else if($data['tiempo']=='30'){
                $cantidad = 'Treinta minutos';
            }else{
                $cantidad='Una hora';
            }
            $data['cantidad'] = $cantidad;
                        
            $data['fecha'] = $date->toDateString();
            $data['hora_inicio'] = Carbon::parse($request->hora_inicio)->toTimeString();
            $data['hora_fin'] = Carbon::parse($request->hora_fin)->toTimeString();

            $usos = Uso_equipo::where('pvd_servicio_id', $pvd_servicio->id)->get();
            
            $hir = Carbon::parse($data['hora_inicio']);
            $hfr = Carbon::parse($data['hora_inicio']);
            
            foreach ($usos as $u) {
                $f = Carbon::parse($u->fecha. '00:00:00');
                $f2 = Carbon::parse($data['fecha']. '00:00:00');

                $hinicio = Carbon::parse($u->hora_inicio);
                $hfin = Carbon::parse($u->hora_fin);

                if ($f->eq($f2) && ($hir->between($hinicio, $hfin) || $hfr->between($hinicio, $hfin) ||
                                    $hinicio->between($hir, $hfr) || $hfin->between($hir, $hfr))) {
                    return response()->json('no');
                }
            }
        }else{
            $data = $request->validate([
                'servicio' => 'required',
                'cantidad' => 'required|Integer',  
                'tarifa' => 'required|Integer',                                                      
            ]);
            
            $data['fecha'] = $date->toDateString();
            $data['hora_inicio'] = '00:00:00';
            $data['hora_fin'] = '00:00:00';
        }

        $data['pvd_servicio_id'] = $pvd_servicio->id;
        $data['tarifa'] = $pvd_servicio->tarifa;
        $data['total'] = $request->tarifa;
        $data['solicitante'] = "Cliente defecto";

        if ($request->factura == 'si') {
            $data['factura'] = $request->factura;
        }
        
        $uso = Uso_equipo::create($data);  
        $data['uso'] = $uso;

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   

    }

    public function pdf($id)
    {   
        $uso = Uso_equipo::find($id);
        $pvd_servicio = $uso->pvd_servicio;
        $pvd = Pvd::find($pvd_servicio->pvd_id);
        $servicio = Servicio::find($pvd_servicio->servicio_id);

        $pdf = new \PDF;
        $view =  \View::make('layouts.pdf.generar_pdf', compact('uso', 'pvd_servicio', 'pvd', 'servicio'))->render();
        $pdf = \PDF::loadHTML($view);
        $pdf->setPaper('A4', 'landscape');

        return $pdf->stream();           
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function es_equipo(Request $request, $id){
        if($request->ajax()){
            $municipio = Municipio::find(Auth::user()->municipio_id);
            $pvd = Pvd::where('municipio_id', '=', $municipio->id)->get();

            $servicios_pvd;
            foreach ($pvd[0]->servicios as $servicio) {
                if ($servicio->id == $id) {
                    $servicios_pvd = $servicio;
                    break;
                }                
            }
            return response()->json($servicios_pvd);
        }
    }
}
