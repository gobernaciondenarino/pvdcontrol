<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Reserva;
use App\Uso_equipo;
use App\Pvd;
use App\Pvd_servicio;


class ReportesController extends Controller
{
    public function reporte_reserva_pvd()
    {
        $pvd = Auth::user()->municipio->pvd;

        $reservas = Reserva::whereHas('sala', function($q) use($pvd) {
            $q->where('pvd_id', $pvd->id);
        })->orderBy('fecha_inicio', 'asc')->orderBy('hora_inicio', 'asc')->get();

        // dd($reservas);
        $factual = Carbon::parse('America/Bogota');
        $factual = $factual->format('Y'.'/'.'m'.'/'.'d');

        $pdf = new \PDF;
        $view =  \View::make('layouts.pdf.reporte_reservas', compact('reservas', 'pvd', 'factual'))->render();
        $pdf = \PDF::loadHTML($view);
        $pdf->setPaper('A4', 'landscape');

        return $pdf->stream();    
    }

    public function reporte_uso_equipo_pvd()
    {
        $pvd = Auth::user()->municipio->pvd;

        $usos = Uso_equipo::whereHas('pvd_servicio', function($q) use($pvd) {
            $q->where('pvd_id', $pvd->id);
        })->orderBy('fecha', 'asc')->orderBy('hora_inicio', 'asc')->get();

        $factual = Carbon::parse('America/Bogota');
        $factual = $factual->format('Y'.'/'.'m'.'/'.'d');

        $pdf = new \PDF;
        $view =  \View::make('layouts.pdf.reporte_uso_equipos', compact('usos', 'pvd', 'factual'))->render();
        $pdf = \PDF::loadHTML($view);
        $pdf->setPaper('A4', 'landscape');

        return $pdf->stream();   
    }

    public function reporte_reserva_total()
    {
        $pvds = Pvd::where('nombre', '!=', 'Pvd global')->get();

        $factual = Carbon::parse('America/Bogota');
        $factual = $factual->format('Y'.'/'.'m'.'/'.'d');

        $pdf = new \PDF;
        $view =  \View::make('layouts.pdf.reporte_reservas_total', compact('pvds', 'factual'))->render();
        $pdf = \PDF::loadHTML($view);
        $pdf->setPaper('A4', 'landscape');

        return $pdf->stream();   
    }

    public function reporte_uso_equipo_total()
    {
        $pvd_servicios = Pvd_servicio::orderBy('pvd_id')->get();
        
        // dd($pvd_servicios[2]->uso_equipos);

        $factual = Carbon::parse('America/Bogota');
        $factual = $factual->format('Y'.'/'.'m'.'/'.'d');

        $pdf = new \PDF;
        $view =  \View::make('layouts.pdf.reporte_uso_equipos_total', compact('pvd_servicios', 'factual'))->render();
        $pdf = \PDF::loadHTML($view);
        $pdf->setPaper('A4', 'landscape');

        return $pdf->stream();  
    }
}
