<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Sala;
use App\Inv_sala;
use App\Inv_detalle_sala;

class SalaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('atras');
        $this->middleware('roleAdminPvd');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $salas = Auth::user()->municipio->pvd->salas->pluck('nombre', 'id');
        return view('layouts.crear_sala.crear_sala', compact('salas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'archivo' => 'required|mimes:xls,xlsx',                         
        ]);

        $pvd = Auth::user()->municipio->pvd->id;

        $file = $data['archivo'];
        $file_name = $file->getClientOriginalName();
        $file->move('files', $file_name);
        
        Excel::load('files/'.$file_name, function($reader) use($pvd){
            foreach ($reader->get() as $r) {
                $data=['nombre' => $r->nombre, 'pvd_id' => $pvd];                
                $sala = Sala::create($data);
            }
        });

        $salas = Auth::user()->municipio->pvd->salas;
        return response()->json($salas); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function inventario(Request $request)
    {
        $data = $request->validate([
            'inventario' => 'required|mimes:xls,xlsx',                         
            'sala' => 'required',                         
        ]);

        $file = $data['inventario'];
        $file_name = $file->getClientOriginalName();
        $file->move('files', $file_name);
        
        Excel::load('files/'.$file_name, function($reader) use($request){
            foreach ($reader->get() as $r) {
                $data=['nombre_elemento' => $r->nombre_elemento, 'unidad' => $r->unidad,
                        'cantidad' => $r->cantidad, 'cantidad_elem_mant_prev' => $r->cantidad_elem_mant_prev,
                        'observaciones' => $r->observaciones, 'sala_id' => $request->sala,
                    ];                
                $inventario = Inv_sala::create($data);

                $data2=['nom_elemento' => $inventario->nombre_elemento, 'tipo_equipo' => $r->tipo_equipo,
                        'marca' => $r->marca, 'serial' => $r->serial,
                        'placa' => $r->placa, 'valor' => $r->valor,
                        'valor_iva' => $r->valor_iva, 'inv_sala_id' => $inventario->id,
                    ];
                $inventario_detalle = Inv_detalle_sala::create($data2);                
            }
        });

        return response()->json('si');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
