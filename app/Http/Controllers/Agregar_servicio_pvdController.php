<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Municipio;
use App\Pvd;
use App\Pvd_servicio;
use App\Servicio;


class Agregar_servicio_pvdController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('atras');
        $this->middleware('roleAdminPvd');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $select_municipio = Municipio::where('id', '=', Auth::user()->municipio->id)->pluck('nombre', 'id');
        $select_pvd = Pvd::where('municipio_id', '=', Auth::user()->municipio->id)->pluck('nombre', 'id');
        $servicios = Servicio::all()->pluck('nombre_servicio', 'id');
        return view('layouts.servicio_pvd.agregar_servicio_pvd', compact('select_municipio', 'select_pvd','servicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mun = Municipio::find(Auth::user()->municipio_id);
        $pvd = Pvd::where('municipio_id','=',$mun->id)->get();
        $serv = Pvd_servicio::where('pvd_id','=',$pvd[0]->id)->get();

        return datatables()
            ->eloquent(Pvd_servicio::where('pvd_id','=',$pvd[0]->id))
            ->addColumn('info', function($serv){
                $nom_serv = Servicio::find($serv->servicio_id);
                return $nom_serv->nombre_servicio;
            })
            ->addColumn('info1', function($serv){
                $a = number_format($serv->tarifa,0,'.', '.');
                return '$ '.$a;
            })
            ->addColumn('action', function($serv){
                return 
                '<div class="row">'.
                '<div class="col text-center">'.
                    '<a href="#" onclick="btn_editar_servicio_pvd('. $serv->id .')" class="btn btn-primary btn-sm editar" ><i class="nav-icon fa fa-edit"></i></a>'.
                    '<a href="#" onclick="btn_eliminar_servicio_pvd('. $serv->id .')" class="btn btn-danger btn-sm eliminar" ><i class="nav-icon fa fa-trash"></i></a>'.
                '</div>'.
                '</div>';
            })  
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'servicio_id' => [
                'required',
                Rule::unique('pvd_servicios')->where(function ($query) {
                $mun = Municipio::find(Auth::user()->municipio_id);
                $pvd = Pvd::where('municipio_id','=',$mun->id)->get();
                    return $query->where('pvd_id', $pvd[0]->id);
                })
            ],
            'tarifa' => 'required|Integer',                                                        
        ]);

        $mun = Municipio::find(Auth::user()->municipio_id);
        $pvd = Pvd::where('municipio_id','=',$mun->id)->get();
        $data['pvd_id'] = $pvd[0]->id;

        Pvd_servicio::create($data);
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio_pvd = Pvd_servicio::find($id);
        return $servicio_pvd;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $servicio_pvd = Pvd_servicio::find($id);    

        $si = $request->validate([
            'servicio_id' => 'required',
            'tarifa' => 'required|Integer',                                                        
        ]);

        $si['id'] = $servicio_pvd->id;

        $servicio_pvd->update($si);

        return response()->json($si);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $servicio_pvd = Pvd_servicio::find($id);

        $count=0;
        $count+=count($servicio_pvd->uso_equipos);
        if($count>0){
            return ['msg'=>'Elemento en uso', 'n' => $count];
        }

        $servicio_pvd->delete();
        return response()->json($servicio_pvd);
    }
}
