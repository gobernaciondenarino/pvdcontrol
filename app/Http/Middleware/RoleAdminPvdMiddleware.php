<?php

namespace App\Http\Middleware;

use Closure;

class RoleAdminPvdMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->role === 'admin_pvd') {
              return $next($request);
        }else{
            abort(403, "¡No tienes permisos para esta seccion!");
        }
    }
}
