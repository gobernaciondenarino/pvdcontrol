<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inv_sala extends Model
{
    
    protected $fillable = [
        'id', 'nombre_elemento', 'unidad', 'cantidad',
        'cantidad_elem_mant_prev', 'observaciones', 'sala_id',
    ];
    
    public $timestamps = false;

    //Un  inventario pertenece a una sala
    public function sala()
	{
	   return $this->belongsTo('App\Sala');
	}

    //Un inventario sala tiene un inventario a detalle
    public function inv_detalle_sala()
    {
        return $this->hasOne('App\Inv_detalle_sala');
    }
}
