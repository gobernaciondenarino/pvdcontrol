<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inv_detalle_sala extends Model
{
	
    protected $fillable = [
        'id', 'nom_elemento', 'tipo_equipo', 
        'marca', 'serial', 'placa',
        'valor', 'valor_iva', 'inv_sala_id',
    ];
    
    public $timestamps = false;

    public function inv_sala()
	{
	   return $this->belongsTo('App\Inv_sala');
	}
}
