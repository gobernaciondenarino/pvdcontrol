<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pvd extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'id', 'codigo', 'nombre',
        'direccion', 'celular',
        'correo', 'municipio_id',
    ];

    //Un pvd pertenece a municipio
    public function municipio()
    {
       return $this->belongsTo('App\Municipio');
    }

    //Un pvd tiene muchas salas
    public function salas()
	{
	   return $this->hasMany('App\Sala');
	}

    // Un pvd tiene muchos servicios
	public function servicios()
    {
       return $this->belongsToMany('App\Servicio', 'pvd_servicios')
                   ->withPivot('id', 'tarifa');
    }

    //Retorna los municipios por el id de un departamento
    public static function pvds($id){
        return Pvd::where('municipio_id','=',$id)
        ->get();
    }
}
