<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvSalasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_salas', function (Blueprint $table) {
           $table->increments('id');
            $table->string('nombre_elemento');
            $table->string('unidad');
            $table->integer('cantidad');
            $table->string('cantidad_elem_mant_prev');
            $table->string('observaciones');

            $table->unsignedInteger('sala_id');
            $table->foreign('sala_id')->references('id')->on('salas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_salas');
    }
}
