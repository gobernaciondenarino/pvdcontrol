<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvDetalleSalasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_detalle_salas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_elemento');
            $table->string('tipo_equipo');
            $table->string('marca');
            $table->string('serial');
            $table->string('placa');
            $table->integer('valor');
            $table->integer('valor_iva');

            $table->unsignedInteger('inv_sala_id');
            $table->foreign('inv_sala_id')->references('id')->on('inv_salas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_detalle_salas');
    }
}
