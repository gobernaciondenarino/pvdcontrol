<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsoEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uso_equipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cantidad')->nullable();
            $table->date('fecha');
            $table->time('hora_inicio')->nullable();
            $table->time('hora_fin')->nullable();
            $table->string('solicitante')->nullable();
            $table->string('tarifa');
            $table->string('total');
            

            $table->unsignedInteger('pvd_servicio_id');
            $table->foreign('pvd_servicio_id')->references('id')->on('pvd_servicios');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uso_equipos');
    }
}
