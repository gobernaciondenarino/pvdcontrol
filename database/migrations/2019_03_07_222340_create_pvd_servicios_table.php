<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePvdServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pvd_servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tarifa');

            $table->unsignedInteger('pvd_id');
            $table->foreign('pvd_id')->references('id')->on('pvds');
            $table->unsignedInteger('servicio_id');
            $table->foreign('servicio_id')->references('id')->on('servicios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pvd_servicios');
    }
}
