<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MunicipiosSeeder::class);
        $this->call(PvdsSeeder::class);
        $this->call(UserSeeder::class);
        // $this->call(ServiciosSeeder::class);
        // $this->call(Pvd_serviciosSeeder::class);
        // $this->call(SalasSeeder::class);
        // $this->call(Inv_salas_Seeder::class);
        // $this->call(Inv_detalle_salas_Seeder::class);
    }
}
