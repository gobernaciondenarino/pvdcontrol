<?php

use Illuminate\Database\Seeder;

class SalasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('salas')->insert([            
            'nombre' => 'Area de entretenimiento',
            'pvd_id' => '2',
        ]);

        DB::table('salas')->insert([            
            'nombre' => 'Centro de producción y contenidos',
            'pvd_id' => '2',
        ]);
    }
}
