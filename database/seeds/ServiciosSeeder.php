<?php

use Illuminate\Database\Seeder;

class ServiciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servicios')->insert([
            'cod_servicio' => '1',
            'nombre_servicio' => 'Portatil HP',
            'equipo' => true,
        ]);

        DB::table('servicios')->insert([
            'cod_servicio' => '2',
            'nombre_servicio' => 'Camara KODAK HD',
            'equipo' => true,

        ]);

        DB::table('servicios')->insert([
            'cod_servicio' => '3',
            'nombre_servicio' => 'Impresion',
            'equipo' => false,

        ]);

        DB::table('servicios')->insert([
            'cod_servicio' => '4',
            'nombre_servicio' => 'Proyector',
            'equipo' => true,

        ]);
    }
}
