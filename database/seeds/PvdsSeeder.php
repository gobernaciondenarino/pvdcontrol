<?php

use Illuminate\Database\Seeder;

class PvdsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
    	DB::table('pvds')->truncate();
    	DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    	
        DB::table('pvds')->insert([
            'codigo' => '1',
            'nombre' => 'Pvd global',
            'direccion' => 'crr1',
            'celular' => '310',
            'correo' => 'asd@123.com',
            'municipio_id' => '1',
        ]);

        // DB::table('pvds')->insert([
        //     'codigo' => '1',
        //     'nombre' => 'Pvd Albán',
        //     'direccion' => 'Crr22 A Calle 3 #24-78',
        //     'celular' => '3122232232',
        //     'correo' => 'asd@123.com',
        //     'municipio_id' => '2',
        // ]);

        // DB::table('pvds')->insert([
        //     'codigo' => '2',
        //     'nombre' => 'Pvd Belén',
        //     'direccion' => 'Crr22 A Calle 3 #24-78',            
        //     'celular' => '3122232232',
        //     'correo' => 'wer@123.com',
        //     'municipio_id' => '3',
        // ]);

        // DB::table('pvds')->insert([
        //     'codigo' => '3',
        //     'nombre' => 'Pvd Buesaco',
        //     'direccion' => 'Crr22 A Calle 3 #24-78',            
        //     'celular' => '3122232232',
        //     'correo' => 'rty@123.com',
        //     'municipio_id' => '4',
        // ]);
    }
}
