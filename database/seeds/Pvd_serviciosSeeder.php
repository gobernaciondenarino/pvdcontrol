<?php

use Illuminate\Database\Seeder;

class Pvd_serviciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pvd_servicios')->insert([
            'tarifa' => '1000',
            'pvd_id' => '2',
            'servicio_id' => '1',
        ]);

        DB::table('pvd_servicios')->insert([
            'tarifa' => '1000',
            'pvd_id' => '2',
            'servicio_id' => '2',

        ]);

        DB::table('pvd_servicios')->insert([
            'tarifa' => '500',
            'pvd_id' => '2',
            'servicio_id' => '3',

        ]);

        DB::table('pvd_servicios')->insert([
            'tarifa' => '1500',
            'pvd_id' => '2',
            'servicio_id' => '4',

        ]);
    }
}
