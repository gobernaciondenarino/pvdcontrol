<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('adminPVD1928'),
            'role' => 'admin',
            'municipio_id' => '1'
        ]);

        // DB::table('users')->insert([
        //     'name' => 'Admin Pvd Alban',
        //     'email' => 'alban@gmail.com',
        //     'password' => bcrypt('123123'),
        //     'role' => 'admin_pvd',
        //     'municipio_id' => '2'
        // ]);

        // DB::table('users')->insert([
        //     'name' => 'Admin Pvd belén',
        //     'email' => 'belen@gmail.com',
        //     'password' => bcrypt('123123'),
        //     'role' => 'admin_pvd',
        //     'municipio_id' => '3'
        // ]);

        // DB::table('users')->insert([
        //     'name' => 'Admin Pvd buesaco',
        //     'email' => 'buesaco@gmail.com',
        //     'password' => bcrypt('123123'),
        //     'role' => 'admin_pvd',
        //     'municipio_id' => '4'
        // ]);
    }
}
