<?php

use Illuminate\Database\Seeder;

class Inv_detalle_salas_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inv_detalle_salas')->insert([            
            'nom_elemento'=> 'Computador de escritorio gama media',
            'tipo_equipo' => 'Computador',
            'marca'=> 'HP',
            'serial'=> '19A60',
            'placa'=> '2345',
            'valor'=> '1200000',
            'valor_iva'=> '2000',
            'inv_sala_id'=> '1'
        ]);
    }
}
