<?php

use Illuminate\Database\Seeder;

class MunicipiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
    	DB::table('municipios')->truncate();
    	DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    	
        DB::table('municipios')->insert([
            'dane' => '0',
            'nombre' => 'No registrado',
        ]);

        // DB::table('municipios')->insert([
        //     'dane' => '1234',
        //     'nombre' => 'Albán',
        // ]);

        // DB::table('municipios')->insert([
        //     'dane' => '4566',
        //     'nombre' => 'Belén',
        // ]);

        // DB::table('municipios')->insert([
        //     'dane' => '4356',
        //     'nombre' => 'Buesaco',
        // ]);
    }
}
