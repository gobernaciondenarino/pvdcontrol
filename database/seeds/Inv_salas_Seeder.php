<?php

use Illuminate\Database\Seeder;

class Inv_salas_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inv_salas')->insert([            
            'nombre_elemento' => 'COMPUTADORES DE ESCRITORIO GAMA MEDIA',
            'unidad' => 'UNIDAD',
            'cantidad' => '15',
            'cantidad_elem_mant_prev' => '15',
            'observaciones' => 'NINGUNA',
            'sala_id'=> '1'
        ]);
    }
}
