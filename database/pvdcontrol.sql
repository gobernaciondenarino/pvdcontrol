/*
SQLyog Enterprise v13.1.1 (64 bit)
MySQL - 10.1.41-MariaDB-0+deb9u1 : Database - pvdcontrol
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pvdcontrol` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `pvdcontrol`;

/*Table structure for table `inv_detalle_salas` */

DROP TABLE IF EXISTS `inv_detalle_salas`;

CREATE TABLE `inv_detalle_salas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom_elemento` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_equipo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marca` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `placa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` int(11) NOT NULL,
  `valor_iva` int(11) NOT NULL,
  `inv_sala_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inv_detalle_salas_inv_sala_id_foreign` (`inv_sala_id`),
  CONSTRAINT `inv_detalle_salas_inv_sala_id_foreign` FOREIGN KEY (`inv_sala_id`) REFERENCES `inv_salas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `inv_detalle_salas` */

/*Table structure for table `inv_salas` */

DROP TABLE IF EXISTS `inv_salas`;

CREATE TABLE `inv_salas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_elemento` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unidad` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `cantidad_elem_mant_prev` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observaciones` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sala_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inv_salas_sala_id_foreign` (`sala_id`),
  CONSTRAINT `inv_salas_sala_id_foreign` FOREIGN KEY (`sala_id`) REFERENCES `salas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `inv_salas` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_02_11_150658_municipios',1),
(2,'2014_10_12_000000_create_users_table',1),
(3,'2014_10_12_100000_create_password_resets_table',1),
(4,'2019_02_11_150717_pvds',1),
(5,'2019_02_11_150741_salas',1),
(6,'2019_02_11_151257_servicios',1),
(7,'2019_03_07_222340_create_pvd_servicios_table',1),
(8,'2019_03_07_222626_create_uso_equipos_table',1),
(9,'2019_03_07_222946_create_inv_salas_table',1),
(10,'2019_04_29_084259_create_inv_detalle_salas_table',1),
(11,'2019_05_03_091752_create_reservas_table',1);

/*Table structure for table `municipios` */

DROP TABLE IF EXISTS `municipios`;

CREATE TABLE `municipios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dane` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `municipios` */

insert  into `municipios`(`id`,`dane`,`nombre`) values 
(1,'0','No registrado');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `pvd_servicios` */

DROP TABLE IF EXISTS `pvd_servicios`;

CREATE TABLE `pvd_servicios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tarifa` int(10) unsigned NOT NULL,
  `pvd_id` int(10) unsigned NOT NULL,
  `servicio_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pvd_servicios_pvd_id_foreign` (`pvd_id`),
  KEY `pvd_servicios_servicio_id_foreign` (`servicio_id`),
  CONSTRAINT `pvd_servicios_pvd_id_foreign` FOREIGN KEY (`pvd_id`) REFERENCES `pvds` (`id`),
  CONSTRAINT `pvd_servicios_servicio_id_foreign` FOREIGN KEY (`servicio_id`) REFERENCES `servicios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `pvd_servicios` */

/*Table structure for table `pvds` */

DROP TABLE IF EXISTS `pvds`;

CREATE TABLE `pvds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` int(11) NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `municipio_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pvds_municipio_id_foreign` (`municipio_id`),
  CONSTRAINT `pvds_municipio_id_foreign` FOREIGN KEY (`municipio_id`) REFERENCES `municipios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `pvds` */

insert  into `pvds`(`id`,`codigo`,`nombre`,`direccion`,`celular`,`correo`,`municipio_id`) values 
(1,1,'Pvd global','crr1','310','asd@123.com',1);

/*Table structure for table `reservas` */

DROP TABLE IF EXISTS `reservas`;

CREATE TABLE `reservas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `solicitante` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facturable` tinyint(1) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fin` time NOT NULL,
  `tarifa` int(11) DEFAULT NULL,
  `sala_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reservas_sala_id_foreign` (`sala_id`),
  CONSTRAINT `reservas_sala_id_foreign` FOREIGN KEY (`sala_id`) REFERENCES `salas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `reservas` */

/*Table structure for table `salas` */

DROP TABLE IF EXISTS `salas`;

CREATE TABLE `salas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pvd_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `salas_pvd_id_foreign` (`pvd_id`),
  CONSTRAINT `salas_pvd_id_foreign` FOREIGN KEY (`pvd_id`) REFERENCES `pvds` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `salas` */

/*Table structure for table `servicios` */

DROP TABLE IF EXISTS `servicios`;

CREATE TABLE `servicios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cod_servicio` int(11) NOT NULL,
  `nombre_servicio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `equipo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `servicios` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `municipio_id` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_municipio_id_foreign` (`municipio_id`),
  CONSTRAINT `users_municipio_id_foreign` FOREIGN KEY (`municipio_id`) REFERENCES `municipios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`role`,`municipio_id`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Administrador','admin@gmail.com',NULL,'$2y$10$BiAdinwmxcs8RkTCk4hEw.qVCXviVq1j/4cKDRJvGUPk7750GZ6n.','admin',1,NULL,NULL,NULL);

/*Table structure for table `uso_equipos` */

DROP TABLE IF EXISTS `uso_equipos`;

CREATE TABLE `uso_equipos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cantidad` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` date NOT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_fin` time DEFAULT NULL,
  `solicitante` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tarifa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pvd_servicio_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uso_equipos_pvd_servicio_id_foreign` (`pvd_servicio_id`),
  CONSTRAINT `uso_equipos_pvd_servicio_id_foreign` FOREIGN KEY (`pvd_servicio_id`) REFERENCES `pvd_servicios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `uso_equipos` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
